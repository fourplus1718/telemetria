package it.di.uniba.sms.fourplus.cliente;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import it.di.uniba.sms.fourplus.cliente.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.cliente.classiDB.Appuntamento;

public class ListAnomaliesAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Anomalia> list;
    private Context context;
    DatabaseReference mDatabase;
    Anomalia anomUndo;
    static TextView tv_appuntamento;
    private ArrayList<Appuntamento> dataApp;

    public ListAnomaliesAdapter(ArrayList<Anomalia> list, Context context, DatabaseReference mDatabase,
                                ArrayList<Appuntamento> dataApp) {
        this.list = list;
        this.context = context;
        this.mDatabase = mDatabase;
        this.dataApp = dataApp;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_list_anomalies, null);
        }

        TextView tv_rilevazione = view.findViewById(R.id.tv_rilevazione);
        tv_rilevazione.setText(view.getResources().getString(R.string.detection_anomaly) + list.get(position).getDataAnomalia() + " " + list.get(position).getoraAnomalia());
        TextView tv_nome_anom = view.findViewById(R.id.tv_nome_anom);
        tv_nome_anom.setText(list.get(position).getTipoAnomalia());
        TextView tv_codice_anom = view.findViewById(R.id.tv_codice_anom);
        tv_codice_anom.setText(view.getResources().getString(R.string.code) + list.get(position).getCodice());
        tv_appuntamento = view.findViewById(R.id.tv_appuntamento);
        final Button btn_segnala = view.findViewById(R.id.btn_segnala);
        final Button btn_ignora = view.findViewById(R.id.btn_ignora);
        final LinearLayout linear_3buttons = view.findViewById(R.id.linear_3buttons);
        final LinearLayout linear_appointment = view.findViewById(R.id.linear_appointment); //inizialmente GONE
        //linear_appointment.setVisibility(View.GONE); lo tolgo perchè è impostato nel layout

        if (list.get(position).isAnomaliaSegnalata() == true) {
            linear_3buttons.setVisibility(View.GONE);
            linear_appointment.setVisibility(View.VISIBLE);
            //tv_appuntamento.setText("Appuntamento: " + list.get(position).getDataAnomalia());
        } else {
            linear_3buttons.setVisibility(View.VISIBLE);
            linear_appointment.setVisibility(View.GONE);
        }

        btn_segnala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                linear_3buttons.setVisibility(View.GONE);
                linear_appointment.setVisibility(View.VISIBLE);

                newAppuntamentoUnconfirmed(position);
                tv_appuntamento.setText(v.getResources().getString(R.string.appointment) + v.getResources().getString(R.string.to_be_confirmed));
                undoAppuntamento(v, list, position, linear_3buttons, linear_appointment);
            }
        });

        btn_ignora.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(final View v) {
                Animation anim = (AnimationUtils.loadAnimation(context, R.anim.slide_out));
                anim.setDuration(500);
                parent.getChildAt(position).startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        //una volta finita l'animazione, elimina la riga
                        updateFlagIgnored(position);
                        anomUndo = list.get(position);
                        list.remove(position);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                        undoIgnored(anomUndo, v, list);
                    }
                });
            }
        });

        boolean f = false;

        //a priori l'appuntamento è da confermare. Se l'appuntamento esiste, prende la sua data e ora
        tv_appuntamento.setText(view.getResources().getString(R.string.appointment) + view.getResources().getString(R.string.to_be_confirmed));

        Anomalia a = (Anomalia) getItem(position);
            for (Appuntamento app : dataApp) {
                if (f == false) {
                    if (a.getCodice().equals(app.getCodiceAnomalia())) {
                        tv_appuntamento.setText(view.getResources().getString(R.string.appointment) + " " + app.getData() +
                        " " + app.getOra());
                        f = true;
                    }
                }
            }

        return view;
    }

    //serve per aggiornare l'anomalia e dire che è stata ignorata
    private void updateFlagIgnored(int pos) {
        Anomalia anom = new Anomalia(list.get(pos).getCodice(), list.get(pos).getTipoAnomalia(), list.get(pos).getValore(),
                list.get(pos).getUnitàMisura(), list.get(pos).getEmailCliente(), list.get(pos).getDataAnomalia(), list.get(pos).getoraAnomalia());
        anom.setAnomaliaIgnorata(true);

        mDatabase.child(context.getString(R.string.anomaly_DB)).child(list.get(pos).getCodice()).setValue(anom);
    }

    //attraverso uno snackbar, questa funzione permette l'annullamento dell'ignoramento delle anomalie
    private void undoIgnored(final Anomalia anomalia, View view, final ArrayList<Anomalia> list) {
        Snackbar.make(view, view.getResources().getString(R.string.ignored_anomaly), Snackbar.LENGTH_LONG)
                .setActionTextColor(view.getResources().getColor(R.color.colorAccent))
                .setAction(view.getResources().getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anomalia.setAnomaliaIgnorata(false);
                        //todo: animazione entrante
                        mDatabase.child(context.getResources().getString(R.string.anomaly_DB)).child(anomalia.getCodice()).setValue(anomalia);
                        list.add(anomalia);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                    }
                })
                .show();
    }

    //serve per aggiornare l'anomalia e dire che è stata ignorata
    private void newAppuntamentoUnconfirmed(int pos) {
        list.get(pos).setAnomaliaSegnalata(true);
        mDatabase.child(context.getResources().getString(R.string.anomaly_DB)).child(list.get(pos).getCodice()).setValue(list.get(pos));
    }

    private void undoAppuntamento(View view, final ArrayList<Anomalia> list, final int pos,
                                  final LinearLayout l_btn, final LinearLayout l_app) {
        Snackbar.make(view, view.getResources().getString(R.string.appointment_notified), Snackbar.LENGTH_LONG)
                .setActionTextColor(view.getResources().getColor(R.color.colorAccent))
                .setAction(context.getResources().getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        l_btn.setVisibility(View.VISIBLE);
                        l_app.setVisibility(View.GONE);

                        list.get(pos).setAnomaliaSegnalata(false);
                        mDatabase.child(context.getString(R.string.anomaly_DB)).child(list.get(pos).getCodice()).setValue(list.get(pos));
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                    }
                })
                .show();
    }



}
