package it.di.uniba.sms.fourplus.cliente;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

import it.di.uniba.sms.fourplus.cliente.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.cliente.classiDB.Appuntamento;

public class TabAnomalieFragment extends Fragment {

    private static Context activity = null, context = null;
    private View view;
    private ListView lv;
    private static ArrayList<Anomalia> dataAnom = new ArrayList<>();
    public DatabaseReference mDatabase;
    private ArrayList<Appuntamento> dataApp = new ArrayList<>();
    static int numNuoveAnomalie = 0;

    public TabAnomalieFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_tab_anomalie, container, false);
        activity = getActivity();
        context = getContext();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        numNuoveAnomalie = 0;

        lv = view.findViewById(R.id.lv_Anomalie);

        DatabaseReference ref = mDatabase.child(getString(R.string.anomaly_DB));
        Query anomalieUtenteQuery = ref.orderByChild(getString(R.string.emailClient_DB)).equalTo(MainActivity.emailUtenteCollegato);
        anomalieUtenteQuery.addValueEventListener(anomalieUtenteListener);

        DatabaseReference ref2 = mDatabase.child(getString(R.string.appointment_DB));
        Query appUtenteQuery = ref2.orderByChild(getString(R.string.emailClient_DB)).equalTo(MainActivity.emailUtenteCollegato);
        appUtenteQuery.addValueEventListener(appUtenteListener);

        return view;
    }

    //listener per effettuare query da database
    ValueEventListener anomalieUtenteListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            numNuoveAnomalie = 0;
            dataAnom.clear();
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                //aggiungi alla lista sono se l'anomalia non è ignorata e se non è già presente in dataAnom
                if (singleSnapshot.getValue(Anomalia.class).isAnomaliaIgnorata() == false &&
                        !dataAnom.contains(singleSnapshot.getValue(Anomalia.class))) {
                    dataAnom.add(singleSnapshot.getValue(Anomalia.class));
                }

                if (singleSnapshot.getValue(Anomalia.class).isAnomaliaIgnorata() == false &&
                        singleSnapshot.getValue(Anomalia.class).isAnomaliaSegnalata() == false) {
                    numNuoveAnomalie++;
                }
            }
            //ordinamento in base alla data e ora del rilevamento della anomalia
            Collections.sort(dataAnom);

            //imposta il badge del tab col numero delle anomalie presenti
            MainActivity.unreadCount[1] = numNuoveAnomalie;
            MainActivity.setupTabIcons();

            ListAnomaliesAdapter adapter = new ListAnomaliesAdapter(dataAnom, context, mDatabase, dataApp);
            lv.setAdapter(adapter);

        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    ValueEventListener appUtenteListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            Appuntamento appuntamento;
            dataApp.clear();
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                appuntamento = singleSnapshot.getValue(Appuntamento.class);
                dataApp.add(appuntamento);
            }

            ListAnomaliesAdapter adapter = new ListAnomaliesAdapter(dataAnom, context, mDatabase, dataApp);
            lv.setAdapter(adapter);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
