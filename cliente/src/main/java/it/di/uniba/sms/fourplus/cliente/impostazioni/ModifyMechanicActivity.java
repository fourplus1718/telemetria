package it.di.uniba.sms.fourplus.cliente.impostazioni;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import it.di.uniba.sms.fourplus.cliente.ListAnomaliesAdapter;
import it.di.uniba.sms.fourplus.cliente.MainActivity;
import it.di.uniba.sms.fourplus.cliente.R;
import it.di.uniba.sms.fourplus.cliente.SpinnerMechanicAdapter;
import it.di.uniba.sms.fourplus.cliente.classiDB.Cliente;
import it.di.uniba.sms.fourplus.cliente.classiDB.Meccanico;

public class ModifyMechanicActivity extends AppCompatActivity {

    Spinner spinner;
    TextView tv_email_meccan, tv_nome_meccan, tv_cognome_meccan, tv_indirizzo_meccan, tv_tel_meccan;
    Button btn_conferma;
    DatabaseReference mDatabase;
    ArrayList<Meccanico> data = new ArrayList<>();
    Context context;
    Cliente cl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_mechanic);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        context = this;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        spinner = findViewById(R.id.spinner);
        tv_email_meccan = findViewById(R.id.tv_email_meccan);
        tv_nome_meccan = findViewById(R.id.tv_nome_meccan);
        tv_cognome_meccan = findViewById(R.id.tv_cognome_meccan);
        tv_indirizzo_meccan = findViewById(R.id.tv_indirizzo_meccan);
        tv_tel_meccan = findViewById(R.id.tv_telefono_meccan);
        btn_conferma = findViewById(R.id.btn_conferma);

        //query per visualizzare tutti i meccanici
        DatabaseReference ref = mDatabase.child(getString(R.string.mechanic_DB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.surname_DB));
        datiUtenteQuery.addListenerForSingleValueEvent(datiUtenteListener);

        btn_conferma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aggiornamento di un record nella tabella users
                updateUser(spinner);
            }
        });
    }

    ValueEventListener datiUtenteListener = new ValueEventListener(){

        //controlla tutti i meccanici presenti nel db
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Meccanico mc = singleSnapshot.getValue(Meccanico.class);
                if (mc != null) {
                    data.add(mc);
                }

                SpinnerMechanicAdapter adapter = new SpinnerMechanicAdapter(data, context, mDatabase);
                spinner.setAdapter(adapter);
                //serve per modificare le info del meccanico in base alla selezione
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        tv_email_meccan.setText(getString(R.string.mechanic_email) + ((Meccanico)spinner.getSelectedItem()).getEmail());
                        tv_nome_meccan.setText(getString(R.string.mechanic_name) + ((Meccanico)spinner.getSelectedItem()).getNome());
                        tv_cognome_meccan.setText(getString(R.string.mechanic_surname) + ((Meccanico)spinner.getSelectedItem()).getCognome());
                        tv_indirizzo_meccan.setText(getString(R.string.mechanic_address) + ((Meccanico)spinner.getSelectedItem()).getIndirizzo());
                        tv_tel_meccan.setText(getString(R.string.mechanic_telephone) + ((Meccanico)spinner.getSelectedItem()).getTelefono());
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private void updateUser(Spinner s) {
        //query per visualizzare i dati già presenti nel db corrispondenti al cliente loggato
        DatabaseReference ref = mDatabase.child(getString(R.string.user_DB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.email_DB)).equalTo(MainActivity.emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(new ValueEventListener(){

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    cl = singleSnapshot.getValue(Cliente.class);
                    String emailMeccanico = ((Meccanico)spinner.getSelectedItem()).getEmail();
                    Cliente clienteModif = cl;
                    clienteModif.setEmailMeccanico(emailMeccanico);

                    mDatabase.child(getString(R.string.user_DB)).child(cl.getTelefono()).setValue(clienteModif);
                    Toast.makeText(context, getString(R.string.mechanic_successfully_changed), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
