package it.di.uniba.sms.fourplus.cliente.impostazioni;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import it.di.uniba.sms.fourplus.cliente.MainActivity;
import it.di.uniba.sms.fourplus.cliente.R;
import it.di.uniba.sms.fourplus.cliente.RegisterActivity;
import it.di.uniba.sms.fourplus.cliente.SpinnerMechanicAdapter;
import it.di.uniba.sms.fourplus.cliente.classiDB.Cliente;
import it.di.uniba.sms.fourplus.cliente.classiDB.Meccanico;

public class ModifyProfileActivity extends AppCompatActivity {

    EditText et_nome;
    EditText et_cognome;
    EditText et_email;
    EditText et_password;
    EditText et_telefono;
    EditText et_modelloVeicolo;
    Button btn_save;
    DatabaseReference mDatabase;
    String emailMeccanico;
    Spinner spinner;
    FirebaseAuth mAuth;
    ArrayList<Meccanico> dataMecc = new ArrayList<>();
    Context context;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        context = this;
        activity = this;

        spinner = findViewById(R.id.spinner);
        et_nome = findViewById(R.id.et_nome);
        et_cognome = findViewById(R.id.et_cognome);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_telefono = findViewById(R.id.et_telefono);
        et_modelloVeicolo = findViewById(R.id.et_modelloVeicolo);
        btn_save = findViewById(R.id.btn_save);

        //query per visualizzare i dati già presenti nel db corrispondenti al cliente loggato
        DatabaseReference ref = mDatabase.child(getString(R.string.user_DB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.email_DB)).equalTo(MainActivity.emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(datiUtenteListener);

        //query per visualizzare tutti i meccanici
        DatabaseReference refMeccanico = mDatabase.child(getString(R.string.mechanic_DB));
        Query datiMeccanicoQuery = refMeccanico.orderByChild(getString(R.string.surname_DB));
        datiMeccanicoQuery.addListenerForSingleValueEvent(datiMeccanicoListener);
    }

    ValueEventListener datiUtenteListener = new ValueEventListener(){

        //controlla se è stato modificato un campo del profilo utente
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Cliente cl = singleSnapshot.getValue(Cliente.class);
                et_nome.setText(cl.getNome());
                et_cognome.setText(cl.getCognome());
                et_email.setText(cl.getEmail());
                et_password.setText(cl.getPassword());
                et_telefono.setText(cl.getTelefono());
                et_modelloVeicolo.setText(cl.getModelloVeicolo());
                //todo et_emailMeccanico.setText(cl.getEmailMeccanico());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    ValueEventListener datiMeccanicoListener = new ValueEventListener() {

        //controlla tutti i meccanici presenti nel db
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Meccanico mc = singleSnapshot.getValue(Meccanico.class);
                if (mc != null) {
                    dataMecc.add(mc);
                }

                SpinnerMechanicAdapter adapter = new SpinnerMechanicAdapter(dataMecc, context, mDatabase);
                spinner.setAdapter(adapter);
                //serve per modificare le info del meccanico in base alla selezione
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        emailMeccanico = ((Meccanico) spinner.getSelectedItem()).getEmail();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }

            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean flag = doModify();
                    if (flag == true) {
                        activity.finish();
                        Toast.makeText(ModifyProfileActivity.this, getString(R.string.modify_ok), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

        private void updateUser(String nome, String cognome, String email, String password, String telefono,
                                String modelloVeicolo, String emailMeccanico) {
        Cliente user = new Cliente(nome, cognome, email, password, telefono, modelloVeicolo, emailMeccanico);

        mDatabase.child(getString(R.string.user_DB)).child(telefono).setValue(user);

        mAuth.getCurrentUser().updateEmail(email);
        mAuth.getCurrentUser().updatePassword(password);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    //metodo per effettuare i controlli sui campi di registrazione e, di conseguenza, la registrazione
    private boolean doModify(){
        boolean isError = false;

        //per azzerare gli errori
        et_nome.setError(null);
        et_cognome.setError(null);
        et_email.setError(null);
        et_password.setError(null);
        et_telefono.setError(null);
        et_modelloVeicolo.setError(null);


        if (et_nome.getText().toString().equals("")){
            et_nome.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_cognome.getText().toString().equals("")){
            et_cognome.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_email.getText().toString().equals("")){
            et_email.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_password.getText().toString().equals("")){
            et_password.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_telefono.getText().toString().equals("")){
            et_telefono.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_modelloVeicolo.getText().toString().equals("")){
            et_modelloVeicolo.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (!isEmailValid(et_email.getText().toString())){
            et_email.setError(getString(R.string.error_invalid_email));
            isError = true;
        }
        if (!isPasswordValid(et_password.getText().toString())){
            et_password.setError(getString(R.string.error_invalid_password));
            isError = true;
        }
        if(isError == false){
            //scrittura di un record nella tabella users
            updateUser(et_nome.getText().toString(), et_cognome.getText().toString(), et_email.getText().toString(),
                    et_password.getText().toString(), et_telefono.getText().toString(), et_modelloVeicolo.getText().toString(),
                    emailMeccanico);
            return true;
        } else {
            return false;
        }

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 6;
    }


}
