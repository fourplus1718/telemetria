package it.di.uniba.sms.fourplus.cliente.impostazioni;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import it.di.uniba.sms.fourplus.cliente.ListAnomaliesAdapter;
import it.di.uniba.sms.fourplus.cliente.ListIgnoredAnomaliesAdapter;
import it.di.uniba.sms.fourplus.cliente.MainActivity;
import it.di.uniba.sms.fourplus.cliente.R;
import it.di.uniba.sms.fourplus.cliente.classiDB.Anomalia;

public class AnomalyIgnoredActivity extends AppCompatActivity {

    private static Context activity = null, context = null;
    private ListView lv;
    public DatabaseReference mDatabase;
    private ArrayList<Anomalia> data = new ArrayList<>();
    static TextView tv_noIgnoredAnomalies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anomaly_ignored);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = getApplicationContext();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        lv = findViewById(R.id.lv_AnomalieIgnorate);
        tv_noIgnoredAnomalies = findViewById(R.id.tv_noIgnoredAnomalies);

        DatabaseReference ref = mDatabase.child(getString(R.string.anomaly_DB));
        Query anomalieIgnorateQuery = ref.orderByChild(getString(R.string.emailClient_DB)).equalTo(MainActivity.emailUtenteCollegato);
        anomalieIgnorateQuery.addValueEventListener(anomalieIgnorateListener);

    }

    //listener per effettuare query da database
    ValueEventListener anomalieIgnorateListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                //aggiungi alla lista solo se l'anomalia non è ignorata e se non è già presente in data
                if (singleSnapshot.getValue(Anomalia.class).isAnomaliaIgnorata() == true &&
                        !data.contains(singleSnapshot.getValue(Anomalia.class))) {
                    data.add(singleSnapshot.getValue(Anomalia.class));
                }

                ListIgnoredAnomaliesAdapter adapter = new ListIgnoredAnomaliesAdapter(data, context, mDatabase);
                lv.setAdapter(adapter);

                if (data.size() == 0) {
                    tv_noIgnoredAnomalies.setVisibility(View.VISIBLE);
                    lv.setVisibility(View.GONE);
                } else {
                    tv_noIgnoredAnomalies.setVisibility(View.GONE);
                    lv.setVisibility(View.VISIBLE);
                }
            }

        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
