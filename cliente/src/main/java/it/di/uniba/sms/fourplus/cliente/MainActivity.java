package it.di.uniba.sms.fourplus.cliente;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import it.di.uniba.sms.fourplus.cliente.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.cliente.classiDB.Cliente;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    MenuItem item_bluetooth = null;
    private String mConnectedDeviceName = null;
    BluetoothOBDService mOBDService;
    BluetoothDevice device = null;
    private BluetoothAdapter mBluetoothAdapter = null;
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    public DatabaseReference mDatabase;
    public static Cliente ut;
    Toolbar toolbarMainActivity;
    Context context;
    static Activity activity;
    public static String emailUtenteCollegato;
    ImageView cintura;
    ImageView img_carburante;

    //variabili per la tab
    private SectionPageAdapter sectionPageAdapter;
    private static ViewPager viewPager;
    public static TabLayout tabLayout;
    static String[] tabTitle;
    public static int[] unreadCount = {0, 0};
    static LayoutInflater li;

    public static FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this; activity = this;
        mAuth = FirebaseAuth.getInstance();
        li = LayoutInflater.from(context); //per poter rendere prepareTabView static

        //Toolbar al posto dell'action menu
        set_toolbar_cliente();

        emailUtenteCollegato = getIntent().getStringExtra("Email");

        tabTitle = new String[]{getResources().getString(R.string.tab_text_1),
                getResources().getString(R.string.tab_text_2)};
        sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.container);
        viewPager.setOffscreenPageLimit(2);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();

        cintura = findViewById(R.id.imageView_cintura);
        img_carburante = findViewById(R.id.imageView_livelloCarburante_icon);

    }

    //funzione che setta il badge e il titolo della singola tab
    private static View prepareTabView(int pos) {
        View view = li.inflate(R.layout.custom_tab,null);
        TextView tv_title = view.findViewById(R.id.tv_title);
        TextView tv_count = view.findViewById(R.id.tv_count);
        tv_title.setText(tabTitle[pos]);
        if(unreadCount[pos]>0)
        {
            tv_count.setVisibility(View.VISIBLE);
            tv_count.setText(""+unreadCount[pos]);
        }
        else
            tv_count.setVisibility(View.GONE);

        return view;
    }

    //questa funzione permette di settare i badge e titoli delle tab
    public static void setupTabIcons() {
        tabLayout.setupWithViewPager(viewPager); //aggiorna la tab ogni volta che viene chiamata questa funzione
        for(int i=0;i<tabLayout.getTabCount();i++) {
            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }
    }

    //questa funzione permette di impostare le pagine della tab
    private void setupViewPager(ViewPager vg) {
        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new TabRealtimeFragment(), getResources().getString(R.string.tab_text_1));
        adapter.addFragment(new TabAnomalieFragment(), getResources().getString(R.string.tab_text_2));
        vg.setAdapter(adapter);
    }

    public int get_bluetooth_state(){
        if(mOBDService == null){
            mOBDService = new BluetoothOBDService(this,mHandler);
        }
        return mOBDService.getState();
    }

    private void set_toolbar_cliente(){
        toolbarMainActivity = findViewById(R.id.toolbar_cliente);
        toolbarMainActivity.setSubtitleTextColor(getResources().getColor(R.color.biancoScuro));
        toolbarMainActivity.setSubtitle("   " + getString(R.string.OBD_not_connected));
        toolbarMainActivity.setLogo(R.mipmap.icona_cliente);
        toolbarMainActivity.setTitle("   " + getString(R.string.app_name));
        setSupportActionBar(toolbarMainActivity);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        item_bluetooth = item;
        switch (item.getItemId()) {
            case R.id.item_Bluetooh:
                if(!item.isCheckable()){
                    item.setCheckable(true);
                    item.setIcon(R.drawable.ic_bluetooth_disabled_black_24dp);
                    //implementazione connessione bluetooth
                    startBluetooth();
                }else{
                    item.setCheckable(false);
                    item.setIcon(R.drawable.ic_bluetooth_searching_black_24dp);
                    //disconnessione bluetooth
                    if(device != null){
                        mOBDService.stop();
                    }else{
                        Toast.makeText(this, R.string.bluetooth_not_enabled,Toast.LENGTH_SHORT).show();
                    }
                }
                return true;
            case R.id.item_settings:
                Intent goSettings = new Intent(this, SettingsActivity.class);
                startActivity(goSettings);
                return true;
            case R.id.item_logout:
                mAuth.signOut();
                Intent goLogin = new Intent(this, LoginActivity.class);
                startActivity(goLogin);
                finish();
                Toast.makeText(context, getString(R.string.logout_done), Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void startBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }else{
                Intent find_devices = new Intent(MainActivity.this, DeviceListActivity.class);
                startActivityForResult(find_devices,REQUEST_CONNECT_DEVICE);
            }
        }
    }

    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        mOBDService = new BluetoothOBDService(this,mHandler);
        // Get the BluetoothDevice object
        device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mOBDService.connect(device, secure);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE:
                // When DeviceListActivity returns with a device to connect
                //boolean connected = false;
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    Intent find_devices = new Intent(MainActivity.this, DeviceListActivity.class);
                    startActivityForResult(find_devices,REQUEST_CONNECT_DEVICE);
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bluetooth_not_enabled,Toast.LENGTH_SHORT).show();
                    item_bluetooth.setCheckable(false);
                    item_bluetooth.setIcon(R.drawable.ic_bluetooth_searching_black_24dp);
                }
        }
    }

    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    @SuppressLint("HandlerLeak") //toglie tutto quel giallo :D
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                //Azioni da compiere nei cambi di stato del Bluetooth
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        //Se connesso, mostra toast e abilita il pulsante bluetooth per la eventuale disconnessione.
                        case BluetoothOBDService.STATE_CONNECTED:
                            Toast.makeText(MainActivity.this, getString(R.string.connect_to)+ " " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                            cintura.setImageResource(R.drawable.ic_no_safety_belt);
                            img_carburante.setImageResource(R.drawable.benz5);
                            item_bluetooth.setEnabled(true);
                            toolbarMainActivity.setSubtitle("   " + getString(R.string.connect_to) + " OBDII");
                            FloatingActionButton fabPlay1 = (FloatingActionButton) findViewById(R.id.fab_Play);
                            fabPlay1.setBackgroundTintList(getResources().getColorStateList(R.color.verdePlay));
                            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                            if ((preferences.getBoolean(getString(R.string.key_automatic_start_OBD),
                                    false)) == true) {
                                fabPlay1.performClick();
                            }
                            break;
                        //Se in connesione, mostra toast e disabilita il pulsante bluetooth durante il tempo di connesione in corso.
                        case BluetoothOBDService.STATE_CONNECTING:
                            cintura = findViewById(R.id.imageView_cintura);
                            img_carburante = findViewById(R.id.imageView_livelloCarburante_icon);
                            cintura.setImageResource(R.drawable.ic_no_safety_belt);
                            img_carburante.setImageResource(R.drawable.benz5);
                            Toast.makeText(MainActivity.this, getString(R.string.connecting)+" ", Toast.LENGTH_SHORT).show();
                            item_bluetooth.setEnabled(false);
                            toolbarMainActivity.setSubtitle("   " + getString(R.string.connecting));
                            FloatingActionButton fabPlay2 = (FloatingActionButton) findViewById(R.id.fab_Play);
                            fabPlay2.setBackgroundTintList(getResources().getColorStateList(R.color.cardview_dark_background));
                            break;
                        case BluetoothOBDService.STATE_LISTEN:
                            break;
                        //Se disconnesso, mostra toast e abilita il pulsante bluetooth per la eventuale connessione.
                        case BluetoothOBDService.STATE_NONE:
                            cintura.setImageResource(R.drawable.ic_no_safety_belt);
                            img_carburante.setImageResource(R.drawable.benz5);
                            item_bluetooth.setEnabled(true);
                            item_bluetooth.setCheckable(false);
                            item_bluetooth.setIcon(R.drawable.ic_bluetooth_searching_black_24dp);
                            toolbarMainActivity.setSubtitle("   " + getString(R.string.OBD_not_connected));
                            //Quando si disconnette il Bluetooth il tasto play cambia colore(disabilitato)
                            FloatingActionButton fabPlay3 = (FloatingActionButton) findViewById(R.id.fab_Play);
                            fabPlay3.setBackgroundTintList(getResources().getColorStateList(R.color.cardview_dark_background));
                            //Quado si disconnette il bluetooth il cronometro si ferma il cronometro
                            Chronometer cronometro = (Chronometer)findViewById(R.id.chronometer);
                            cronometro.stop();
                            break;
                    }
                    break;

                case Constants.MESSAGE_WRITE:
                    //case vuoto poichè il client bluetooth non deve scrivere nulla, ma deve solo ricevere i dati dall' OBD.
                    break;

                //cattura i dati letti dall'OBD
                case Constants.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    //construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //andare a settare i dati di interesse nella UI e salvare le anomalie nel database.
                    String[] read = readMessage.split(",");
                    FloatingActionButton fabPlay4 = (FloatingActionButton) findViewById(R.id.fab_Play);
                    ImageView cintura = findViewById(R.id.imageView_cintura);

                    // se il tasto fabPlay è stato pigiato, vengono mostrati i dati in tempo reale
                    if(!fabPlay4.isEnabled()){
                        String velocita = read[0];
                        String livelloCarburante = read[1];
                        String consumo = read[2];
                        String autonomia = read[3];
                        String statoCintura = read[4];
                        String distanza_percorsa = read[5];
                        String temperaturaMotore = read[6];
                        String codiceAnomalia = read[7].replace("\n","");

                        //se ha ricevuto una anomalia, la va a scrivere nel database
                        if(!codiceAnomalia.equals("")){
                            String tipo = read[8];
                            String valore = read[9];
                            String unitaMisura = read[10];
                            String data = read[11];
                            String ora = read[12].replace("\n","");
                            mDatabase = FirebaseDatabase.getInstance().getReference();
                            Anomalia mAnomalia = new Anomalia(codiceAnomalia,tipo,valore,unitaMisura,MainActivity.emailUtenteCollegato,data,ora);
                            mDatabase.child(getString(R.string.anomaly_DB)).child(codiceAnomalia).setValue(mAnomalia);
                        }

                        TextView textLivelloCarburante = (TextView)findViewById(R.id.textView_valoreLivelloCarburante);
                        TextView textVelocita = (TextView)findViewById(R.id.textView_velocità);
                        TextView textConsumo = (TextView)findViewById(R.id.textView_valoreConsumoCarburante);
                        TextView textAutonomia = (TextView)findViewById(R.id.textView_valoreAutonomia);
                        TextView textDistanza = (TextView)findViewById(R.id.textView_valoreDistanzaPercorsa);
                        TextView textTemperatura = (TextView) findViewById(R.id.textView_valoreTemperaturaMotore);

                        //setta i campi in tempo reale della schermata principale
                        if(statoCintura.equals("false")){
                            cintura.setImageResource(R.drawable.ic_no_safety_belt);
                        } else {
                            cintura.setImageResource(R.drawable.ic_safety_belt);
                        }
                        textLivelloCarburante.setText((livelloCarburante+"%"));
                        textVelocita.setText(velocita+" km/h");
                        textConsumo.setText(consumo+" " + getResources().getQuantityString(R.plurals.liters,
                                Integer.parseInt(consumo), Integer.parseInt(consumo)));
                        textAutonomia.setText(autonomia+" km");
                        textDistanza.setText(distanza_percorsa+" km");
                        textTemperatura.setText(temperaturaMotore+" °C");

                        int carburanteInt = Integer.parseInt(livelloCarburante);
                        if (carburanteInt >= 0 && carburanteInt <=10) {
                            img_carburante.setImageResource(R.drawable.benz5);
                        } else if (carburanteInt >= 11 && carburanteInt <=25) {
                            img_carburante.setImageResource(R.drawable.benz4);
                        } else if (carburanteInt >= 26 && carburanteInt <=50) {
                            img_carburante.setImageResource(R.drawable.benz3);
                        } else if (carburanteInt >= 51 && carburanteInt <=80) {
                            img_carburante.setImageResource(R.drawable.benz2);
                        } else if (carburanteInt >= 81 && carburanteInt <=100) {
                            img_carburante.setImageResource(R.drawable.benz1);
                        }
                    }
                    break;

                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    Toast.makeText(MainActivity.this, getString(R.string.connect_to)+ " " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;

                case Constants.MESSAGE_TOAST:
                    Toast.makeText(MainActivity.this, msg.getData().getString(Constants.TOAST),Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }
}
