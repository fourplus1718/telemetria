package it.di.uniba.sms.fourplus.cliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import it.di.uniba.sms.fourplus.cliente.classiDB.Cliente;
import it.di.uniba.sms.fourplus.cliente.classiDB.Meccanico;

public class RegisterActivity extends AppCompatActivity {

    public static int PICK_IMAGE_REQUEST = 0;
    ImageView img_cliente_reg;
    Button btn_picker;
    EditText et_nome;
    EditText et_cognome;
    EditText et_email;
    EditText et_password;
    EditText et_telefono;
    EditText et_modelloVeicolo;
    String emailMeccanico;
    Spinner spinner;
    Button btn_register;
    private FirebaseAuth mAuth;
    //FirebaseUser currentUser;
    DatabaseReference mDatabase;
    ArrayList<Meccanico> data = new ArrayList<>();
    static Context context;
    static Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        context = this;
        activity = this;
        //currentUser = FirebaseAuth.getInstance().getCurrentUser();

        //currentUser = mAuth.getCurrentUser();
        spinner = findViewById(R.id.spinner);
        et_nome = findViewById(R.id.et_nome);
        et_cognome = findViewById(R.id.et_cognome);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_telefono = findViewById(R.id.et_telefono);
        et_modelloVeicolo = findViewById(R.id.et_modelloVeicolo);
        btn_register = findViewById(R.id.btn_register);
        //img_cliente_reg = findViewById(R.id.img_cliente_reg);
        //btn_picker = findViewById(R.id.btn_picker);
        /*btn_picker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pressPicker();
            }
        });*/

        //query per visualizzare tutti i meccanici
        DatabaseReference refMeccanico = mDatabase.child(getString(R.string.mechanic_DB));
        Query datiMeccanicoQuery = refMeccanico.orderByChild(getString(R.string.surname_DB));
        datiMeccanicoQuery.addListenerForSingleValueEvent(datiMeccanicoListener);
    }

    /*private void pressPicker() {
        //intent implicito per far scegliere la foto
        Intent i = new Intent(Intent.ACTION_PICK);
        i.setType("image/*");
        startActivityForResult(i, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data.getData() != null) {
            //imposta l'immagine scelta
            img_cliente_reg.setImageURI(data.getData());
        }
    }*/

    ValueEventListener datiMeccanicoListener = new ValueEventListener(){

        //controlla tutti i meccanici presenti nel db
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Meccanico mc = singleSnapshot.getValue(Meccanico.class);
                if (mc != null) {
                    data.add(mc);
                }

                SpinnerMechanicAdapter adapter = new SpinnerMechanicAdapter(data, context, mDatabase);
                spinner.setAdapter(adapter);
                //serve per modificare le info del meccanico in base alla selezione
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                        emailMeccanico = ((Meccanico)spinner.getSelectedItem()).getEmail();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {

                    }

                });
            }

                btn_register.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean flag = doRegister();
                        if (flag == true) {
                            activity.finish();
                            /*Snackbar.make(v, getString(R.string.registration_ok), Snackbar.LENGTH_LONG)
                                    .setActionTextColor(context.getResources().getColor(R.color.colorAccent))
                                    .show();*/
                            Toast.makeText(RegisterActivity.this, getString(R.string.registration_ok), Toast.LENGTH_LONG).show();
                        }
                    }
                });
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };


    //salva l'utente in authentication
    private void registerUser(EditText et_email, EditText et_password) {
        mAuth.createUserWithEmailAndPassword(et_email.getText().toString(), et_password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            mAuth.getCurrentUser();
                        } else {
                            // If sign in fails, display a message to the user.
                            //updateUI(null);
                        }
                    }
                });
    }

    //scrittura di un record nella tabella Users
    //salva l'utente nel database
    private void writeNewUser(String nome, String cognome, String email, String password, String telefono,
                              String modelloVeicolo, String emailMeccanico) {
        Cliente user = new Cliente(nome, cognome, email, password, telefono, modelloVeicolo, emailMeccanico);

        mDatabase.child("utente").child(telefono).setValue(user);
    }

    //metodo per effettuare i controlli sui campi di registrazione e, di conseguenza, la registrazione
    private boolean doRegister(){
        boolean isError = false;

        //per azzerare gli errori
        et_nome.setError(null);
        et_cognome.setError(null);
        et_email.setError(null);
        et_password.setError(null);
        et_telefono.setError(null);
        et_modelloVeicolo.setError(null);


        if (et_nome.getText().toString().equals("")){
            et_nome.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_cognome.getText().toString().equals("")){
            et_cognome.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_email.getText().toString().equals("")){
            et_email.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_password.getText().toString().equals("")){
            et_password.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_telefono.getText().toString().equals("")){
            et_telefono.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (et_modelloVeicolo.getText().toString().equals("")){
            et_modelloVeicolo.setError(getString(R.string.error_field_required));
            isError = true;
        }
        if (!isEmailValid(et_email.getText().toString())){
            et_email.setError(getString(R.string.error_invalid_email));
            isError = true;
        }
        if (!isPasswordValid(et_password.getText().toString())){
            et_password.setError(getString(R.string.error_invalid_password));
            isError = true;
        }
        if(isError == false){
            registerUser(et_email, et_password);

            //scrittura di un record nella tabella users
            writeNewUser(et_nome.getText().toString(), et_cognome.getText().toString(), et_email.getText().toString(),
                    et_password.getText().toString(), et_telefono.getText().toString(), et_modelloVeicolo.getText().toString(),
                    emailMeccanico);
            return true;
        } else {
            return false;
        }

    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 6;
    }

}

//Firebase Database paths must not contain '.', '#', '$', '[', or ']'