package it.di.uniba.sms.fourplus.cliente;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Calendar;

import it.di.uniba.sms.fourplus.cliente.classiDB.Anomalia;

public class ListIgnoredAnomaliesAdapter extends BaseAdapter implements ListAdapter {

    private ArrayList<Anomalia> list;
    private Context context;
    DatabaseReference mDatabase;
    Anomalia anomUndo;

    public ListIgnoredAnomaliesAdapter(ArrayList<Anomalia> list, Context context, DatabaseReference mDatabase) {
        this.list = list;
        this.context = context;
        this.mDatabase = mDatabase;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_list_ignored_anomalies, null);
        }

        TextView tv_nome_anom = view.findViewById(R.id.tv_nome_anom);
        tv_nome_anom.setText(list.get(position).getTipoAnomalia());
        TextView tv_codice_anom = view.findViewById(R.id.tv_codice_anom);
        tv_codice_anom.setText(context.getString(R.string.code) + list.get(position).getCodice());
        TextView tv_rilevazione = view.findViewById(R.id.tv_rilevazione_anom);
        tv_rilevazione.setText(context.getString(R.string.detection_anomaly) + list.get(position).getDataAnomalia() +
                list.get(position).getoraAnomalia());
        final Button btn_nonIgnorare = view.findViewById(R.id.btn_nonIgnorare);

        btn_nonIgnorare.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(final View v) {
                Animation anim = (AnimationUtils.loadAnimation(context, R.anim.slide_out)); //todo: oppure fade out
                anim.setDuration(500);
                parent.getChildAt(position).startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        //una volta finita l'animazione, elimina la riga
                        updateFlagNotIgnored(position);
                        anomUndo = list.get(position);
                        list.remove(position);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                        undoIgnored(anomUndo, v, list);
                    }
                });
            }
        });

        return view;
    }

    //serve per aggiornare l'anomalia e dire che è da riaggungere tra le anomalie da considerare
    private void updateFlagNotIgnored(int pos) {
        Anomalia anom = new Anomalia(list.get(pos).getCodice(), list.get(pos).getTipoAnomalia(), list.get(pos).getValore(),
                list.get(pos).getUnitàMisura(), list.get(pos).getEmailCliente(), list.get(pos).getDataAnomalia(), list.get(pos).getoraAnomalia());
        anom.setAnomaliaIgnorata(false);

        mDatabase.child(context.getString(R.string.anomaly_DB)).child(list.get(pos).getCodice()).setValue(anom);
    }

    //attraverso uno snackbar, questa funzione permette l'annullamento della visibilità dell'anomalia
    private void undoIgnored(final Anomalia anomalia, View view, final ArrayList<Anomalia> list) {
        Snackbar.make(view, context.getString(R.string.anomaly_visible), Snackbar.LENGTH_LONG)
                .setActionTextColor(view.getResources().getColor(R.color.colorAccent))
                .setAction(context.getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anomalia.setAnomaliaIgnorata(true);
                        //todo: animazione entrante
                        mDatabase.child(context.getString(R.string.anomaly_DB)).child(anomalia.getCodice()).setValue(anomalia);
                        list.add(anomalia);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                    }
                })
                .show();
    }

}
