package it.di.uniba.sms.fourplus.cliente;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import ru.dimorinny.floatingtextbutton.FloatingTextButton;

public class TabRealtimeFragment extends Fragment {
    static ImageView img_CinturaSicurezza;
    static TextView tv_Velocità;
    static ImageView img_LivelloCarburante;
    static TextView tv_ValoreLivelloCarburante;
    static TextView tv_Autonomia;
    static TextView tv_ConsumoCarburante;
    static TextView tv_DistanzaPercorsa;
    static TextView tv_TemperaturaMotore;
    static TextView tv_ValoreAutonomia;
    static TextView tv_ValoreConsumoCarburante;
    static TextView tv_ValoreDistanzaPercorsa;
    static TextView tv_ValoreTemperaturaMotore;
    FloatingActionButton fabPlay;
    FloatingTextButton btnEnd, btnPause;
    static Chronometer cronometro;

    View view;
    private static Context activity = null, context = null;
    private static long pauseOffset = 0;
    private static boolean running = false;
    private BluetoothAdapter mBluetoothAdapter = null;

    final static String BACKGROUND_COLOR = "backgroundColor";


    public TabRealtimeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = getActivity();
        context = getContext();

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tab_realtime, container, false);
        img_CinturaSicurezza = view.findViewById(R.id.imageView_cintura);
        tv_Velocità = view.findViewById(R.id.textView_velocità);
        img_LivelloCarburante = view.findViewById(R.id.imageView_livelloCarburante);
        tv_ValoreLivelloCarburante = view.findViewById(R.id.textView_valoreLivelloCarburante);
        tv_Autonomia = view.findViewById(R.id.textView_autonomia);
        tv_ConsumoCarburante = view.findViewById(R.id.textView_consumoCarburante);
        tv_DistanzaPercorsa = view.findViewById(R.id.textView_distanzaPercorsa);
        tv_TemperaturaMotore = view.findViewById(R.id.textView_temperaturaMotore);
        tv_ValoreAutonomia = view.findViewById(R.id.textView_valoreAutonomia);
        tv_ValoreConsumoCarburante = view.findViewById(R.id.textView_valoreConsumoCarburante);
        tv_ValoreDistanzaPercorsa = view.findViewById(R.id.textView_valoreDistanzaPercorsa);
        tv_ValoreTemperaturaMotore = view.findViewById(R.id.textView_valoreTemperaturaMotore);
        cronometro = view.findViewById(R.id.chronometer);

        fabPlay = view.findViewById(R.id.fab_Play);
        fabPlay.setBackgroundTintList(getResources().getColorStateList(R.color.cardview_dark_background));
        btnPause = view.findViewById(R.id.btn_pause);
        btnPause.setTitle(getString(R.string.pause) + "         ");
        btnEnd = view.findViewById(R.id.btn_stop);
        btnEnd.setTitle("           " + getString(R.string.stop));

        //se nelle impostazioni è settato avvio automatico, viene simulata la pressione del tasto play
        if (((MainActivity)getActivity()).get_bluetooth_state() != BluetoothOBDService.STATE_CONNECTED) {
            Toast.makeText(getActivity(), getString(R.string.OBD_not_connected), Toast.LENGTH_SHORT).show();
        } else {
            if ((preferences.getBoolean(getString(R.string.key_automatic_start_OBD), true)) == true) {
                fabPlay.performClick();
            }
        }

        fabPlay.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                if (!fabPlay.getBackgroundTintList().equals(R.color.cardview_dark_background)) {
                    if (((MainActivity) getActivity()).get_bluetooth_state() != BluetoothOBDService.STATE_CONNECTED) {
                        Toast.makeText(getActivity(), getString(R.string.OBD_not_connected), Toast.LENGTH_SHORT).show();
                    } else {
                        if ((preferences.getBoolean(getString(R.string.key_display_on), true)) == true) {
                            //questa riga consente di mantenere lo schermo acceso
                            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        }
                        if (!running) {
                            cronometro.setBase(SystemClock.elapsedRealtime() - pauseOffset);
                            cronometro.start();
                            running = true;
                        }
                        btnEnd.setEnabled(true);
                        btnEnd.setClickable(true);
                        btnPause.setEnabled(true);
                        btnPause.setClickable(true);
                        fabPlay.setEnabled(false);
                        fabPlay.setBackgroundTintList(getResources().getColorStateList(R.color.cardview_dark_background));
                        fabPlay.setElevation(8);
                        //animazione per cambio colore
                        AnimatorSet set = new AnimatorSet();
                        set.playTogether(
                                ObjectAnimator.ofObject(btnEnd, BACKGROUND_COLOR, new ArgbEvaluator(),
                                        0xFF808080, 0xFFd50000)
                                        .setDuration(300),
                                ObjectAnimator.ofObject(btnPause, BACKGROUND_COLOR, new ArgbEvaluator(),
                                        0xFF808080, 0xFFf57c00)
                                        .setDuration(300)
                        );
                        set.start();
                    }
                }
            }
        });
        btnPause.setEnabled(false);
        btnPause.setBackgroundColor(getResources().getColor(R.color.grigioDisable));
        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnEnd.getBackgroundColor() != getResources().getColor(R.color.grigioDisable)) {
                    if (btnPause.getBackgroundColor() != getResources().getColor(R.color.grigioDisable)) {
                        if (((MainActivity) getActivity()).get_bluetooth_state() != BluetoothOBDService.STATE_CONNECTED) {
                            //Toast.makeText(getActivity(), getString(R.string.OBD_not_connected), Toast.LENGTH_SHORT).show();
                        } else {
                            fabPlay.setBackgroundTintList(getResources().getColorStateList(R.color.verdePlay));

                            if ((preferences.getBoolean(getString(R.string.key_display_on), true)) == true) {
                                //questa riga consente di mantenere lo schermo acceso
                                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                            }
                            if (running) {
                                cronometro.stop();
                                pauseOffset = SystemClock.elapsedRealtime() - cronometro.getBase();
                                running = false;
                            }
                            btnEnd.setEnabled(true);
                            btnEnd.setClickable(true);
                            btnPause.setEnabled(false);
                            btnPause.setClickable(false);
                            fabPlay.setEnabled(true);
                            //animazione per cambio colore
                            AnimatorSet set = new AnimatorSet();
                            set.playTogether(
                                    ObjectAnimator.ofObject(btnPause, BACKGROUND_COLOR, new ArgbEvaluator(),
                                            0xFFf57c00, 0xFF808080)
                                            .setDuration(300)
                            );
                            set.start();
                        }
                    }
                }
            }
        });
        btnEnd.setEnabled(false);
        btnEnd.setBackgroundColor(getResources().getColor(R.color.grigioDisable));
        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnEnd.getBackgroundColor() != getResources().getColor(R.color.grigioDisable)) {
                    if (((MainActivity) getActivity()).get_bluetooth_state() != BluetoothOBDService.STATE_CONNECTED) {
                        //Toast.makeText(getActivity(), getString(R.string.OBD_not_connected), Toast.LENGTH_SHORT).show();
                    } else {
                        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        tv_ValoreAutonomia.setText(String.valueOf((SystemClock.elapsedRealtime() - cronometro.getBase()) / 1000));
                        cronometro.stop();
                        cronometro.setBase(SystemClock.elapsedRealtime());
                        running = false;
                        pauseOffset = 0;
                        btnEnd.setEnabled(false);
                        btnEnd.setClickable(false);
                        btnPause.setEnabled(false);
                        btnPause.setClickable(false);
                        fabPlay.setEnabled(true);
                        fabPlay.setBackgroundTintList(getResources().getColorStateList(R.color.verdePlay));

                        //quando si clicca STOP si azzerano tutte le textView dei dati in realtime
                        img_CinturaSicurezza.setImageResource(R.drawable.ic_no_safety_belt);
                        tv_ValoreLivelloCarburante.setText(getString(R.string.value_fuel));
                        tv_ValoreAutonomia.setText(getString(R.string.value_autonomy));
                        tv_ValoreConsumoCarburante.setText(getString(R.string.value_fuel_consumption));
                        tv_ValoreDistanzaPercorsa.setText(getString(R.string.value_distance_traveled));
                        tv_ValoreTemperaturaMotore.setText(getString(R.string.value_engine_temperature));
                        tv_Velocità.setText(getString(R.string.value_speed));

                        //animazione per cambio colore
                        AnimatorSet set = new AnimatorSet();
                        set.playTogether(
                                ObjectAnimator.ofObject(btnEnd, BACKGROUND_COLOR, new ArgbEvaluator(),
                                        0xFFd50000, 0xFF808080)
                                        .setDuration(300),
                                ObjectAnimator.ofObject(btnPause, BACKGROUND_COLOR, new ArgbEvaluator(),
                                        0xFFf57c00, 0xFF808080)
                                        .setDuration(300)
                        );
                        set.start();
                    }
                }
            }
        });

        return view;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

    }

}


