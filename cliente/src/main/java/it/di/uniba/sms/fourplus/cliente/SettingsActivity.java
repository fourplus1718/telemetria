package it.di.uniba.sms.fourplus.cliente;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.di.uniba.sms.fourplus.cliente.classiDB.Cliente;

public class SettingsActivity extends AppCompatActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();
    public DatabaseReference mDatabase;
    TextView tv_nome_cliente;
    TextView tv_email_cliente;
    ImageView img_cliente;
    static Context context;
    static Activity activity;
    static FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dati_cliente_settings);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        context = this;
        activity = this;
        mAuth = FirebaseAuth.getInstance();

        //getLayoutInflater().inflate(R.layout.dati_cliente_settings, (ViewGroup) findViewById(android.R.id.content));
        tv_nome_cliente = findViewById(R.id.nome_cliente);
        tv_email_cliente = findViewById(R.id.email_cliente);
        tv_email_cliente.setText(MainActivity.emailUtenteCollegato);
        img_cliente = findViewById(R.id.img_cliente);
        img_cliente.setImageResource(R.drawable.ic_person_black_24dp);

        getFragmentManager().beginTransaction().replace(R.id.content_frame, new MainPreferenceFragment()).commit();

        DatabaseReference ref = mDatabase.child(getString(R.string.client_DB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.email_DB)).equalTo(MainActivity.emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(datiUtenteListener);
    }

    ValueEventListener datiUtenteListener = new ValueEventListener(){

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Cliente cl = singleSnapshot.getValue(Cliente.class);
                tv_nome_cliente.setText(cl.getNome() + " " + cl.getCognome());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public static class MainPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_main);

            Preference btn_logout = findPreference(getString(R.string.key_logout));
            btn_logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    mAuth.signOut();
                    Intent goLogin = new Intent(activity, LoginActivity.class);
                    startActivity(goLogin);
                    activity.finish();
                    MainActivity.activity.finish();
                    Toast.makeText(context, getString(R.string.logout_done), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
