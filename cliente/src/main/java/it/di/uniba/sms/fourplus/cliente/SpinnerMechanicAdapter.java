package it.di.uniba.sms.fourplus.cliente;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

import it.di.uniba.sms.fourplus.cliente.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.cliente.classiDB.Meccanico;

public class SpinnerMechanicAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Meccanico> list;
    private Context context;
    DatabaseReference mDatabase;

    public SpinnerMechanicAdapter(ArrayList<Meccanico> list, Context context, DatabaseReference mDatabase) {
        this.list = list;
        this.context = context;
        this.mDatabase = mDatabase;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_spinner_mechanic, null);
        }

        TextView tv_nome_meccan = view.findViewById(R.id.rowtext);
        tv_nome_meccan.setText(list.get(position).getCognome() + " " + list.get(position).getNome());

        if (context.getClass().toString().contains("RegisterActivity") ||
                context.getClass().toString().contains("ModifyProfileActivity")) {
            tv_nome_meccan.setBackgroundColor(view.getResources().getColor(R.color.trasparente));
            tv_nome_meccan.setTextColor(view.getResources().getColor(R.color.nero));
            tv_nome_meccan.setTextSize(20);
        }

        return view;
    }

}
