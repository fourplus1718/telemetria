package it.di.uniba.sms.fourplus.cliente.classiDB;

public class Appuntamento {
    private String codiceAppuntamento;
    private String data;
    private String ora;
    private String emailMeccanico;
    private String emailCliente;
    private String codiceAnomalia;

    public Appuntamento() {

    }

    public Appuntamento(String data, String ora, String emailMeccanico, String emailCliente, String codiceAnomalia, String codiceAppuntamento) {
        this.codiceAppuntamento = codiceAppuntamento;
        this.data = data;
        this.ora = ora;
        this.emailMeccanico = emailMeccanico;
        this.emailCliente = emailCliente;
        this.codiceAnomalia = codiceAnomalia;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOra() {
        return ora;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public String getEmailMeccanico() {
        return emailMeccanico;
    }

    public void setEmailMeccanico(String emailMeccanico) {
        this.emailMeccanico = emailMeccanico;
    }

    public String getEmailCliente() {
        return emailCliente;
    }

    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    public String getCodiceAnomalia() {
        return codiceAnomalia;
    }

    public void setCodiceAnomalia(String codiceAnomalia) {
        this.codiceAnomalia = codiceAnomalia;
    }

    public String getCodiceAppuntamento() {
        return codiceAppuntamento;
    }
    public void setCodiceAppuntamento(String codiceAppuntamento) {
        this.codiceAppuntamento = codiceAppuntamento;
    }

    public String toString(){
        return  "Ora: "+ora+"\n"+
                "Cliente: "+emailCliente+"\n"+
                "Codice Anomalia: "+codiceAnomalia+"\n"+
                "CodiceAppuntamento: "+ codiceAppuntamento;
    }
}
