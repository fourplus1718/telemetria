package it.di.uniba.sms.fourplus.cliente.impostazioni;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.di.uniba.sms.fourplus.cliente.MainActivity;
import it.di.uniba.sms.fourplus.cliente.R;
import it.di.uniba.sms.fourplus.cliente.classiDB.Cliente;

public class DeleteProfileActivity extends AppCompatActivity {

    TextView tv_nome;
    TextView tv_cognome;
    TextView tv_email;
    TextView tv_telefono;
    TextView tv_modelloVeicolo;
    TextView tv_emailMeccanico;
    Button btn_delete;

    Context context;
    public static FirebaseAuth mAuth;
    DatabaseReference mDatabase;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        tv_nome = findViewById(R.id.tv_nome);
        tv_cognome = findViewById(R.id.tv_cognome);
        tv_email = findViewById(R.id.tv_email);
        tv_telefono = findViewById(R.id.tv_telefono);
        tv_modelloVeicolo = findViewById(R.id.tv_modelloVeicolo);
        tv_emailMeccanico = findViewById(R.id.tv_emailMeccanico);
        //query per visualizzare i dati già presenti nel db corrispondenti al cliente loggato
        DatabaseReference ref = mDatabase.child(getString(R.string.user_DB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.email_DB)).equalTo(MainActivity.emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    Cliente cl = singleSnapshot.getValue(Cliente.class);
                    tv_nome.setText(getString(R.string.name) + ": " + cl.getNome());
                    tv_cognome.setText(getString(R.string.surname) + ": " + cl.getCognome());
                    tv_email.setText(getString(R.string.email) + ": " + cl.getEmail());
                    tv_telefono.setText(getString(R.string.telephone) + ": " + cl.getTelefono());
                    tv_modelloVeicolo.setText(getString(R.string.vehicle_model) + ": " + cl.getModelloVeicolo());
                    tv_emailMeccanico.setText(getString(R.string.email_mechanic) + ": " + cl.getEmailMeccanico());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btn_delete = findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               deleteUser();
                Toast.makeText(context, getString(R.string.elimination_completed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void deleteUser() {
        mDatabase.child(getString(R.string.user_DB)).child(tv_telefono.toString()).removeValue();
        mAuth.getCurrentUser().delete();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
