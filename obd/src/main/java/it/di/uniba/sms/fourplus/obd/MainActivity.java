package it.di.uniba.sms.fourplus.obd;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.util.TimeUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilterWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivityOBDII";

    /**
     * Name of the connected device
     */
    private String mConnectedDeviceName = null;
    private static final int REQUEST_ENABLE_BT = 2;
    TextView text;
    BluetoothAdapter mBluetoothAdapter;
    static final String fileNameData = "OBD_data.csv";

    BluetoothOBDServiceServer mBluetoothOBDServer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = (TextView) findViewById(R.id.text);
        text.setText("");

        text.setText("OBDII Server Data");
        checkBluetooth();

        // GENERAZIONE FILE CSV dei dati realtime

        Random x = new Random();

        //Anomalie
        String codice = x.nextInt(5000000)+"";
        String tipoAnomalia = "Alta pressione carburante";
        String valore = 700+"";
        String unitaMisura = "kPa";
        String data = "21-6-2018";
        String ora = "11:15";
        String anomalia_1 = codice + "," +tipoAnomalia + "," +valore + "," +unitaMisura + "," +data + "," +ora;

        codice = x.nextInt(5000000)+"";
        tipoAnomalia = "Problema giri motore";
        valore = 12000+"";
        unitaMisura = "rpm";
        data = "21-6-2018";
        ora = "11:20";
        String anomalia_2 = codice + "," +tipoAnomalia + "," +valore + "," +unitaMisura + "," +data + "," +ora;

        codice = x.nextInt(5000000)+"";
        tipoAnomalia = "Temperatura liquido raffreddamento";
        valore = 200+"";
        unitaMisura = "°C";
        data = "21-6-2018";
        ora = "11:22";
        String anomalia_3 = codice + "," +tipoAnomalia + "," +valore + "," +unitaMisura + "," +data + "," +ora;

        codice = x.nextInt(5000000)+"";
        tipoAnomalia = "Temperatura olio motore";
        valore = 190+"";
        unitaMisura = "°C";
        data = "21-6-2018";
        ora = "11:30";
        String anomalia_4 = codice + "," +tipoAnomalia + "," +valore + "," +unitaMisura + "," +data + "," +ora;

        codice = x.nextInt(5000000)+"";
        tipoAnomalia = " Tensione batteria";
        valore = 65+"";
        unitaMisura = "Volt";
        data = "21-6-2018";
        ora = "11:35";
        String anomalia_5 = codice + "," +tipoAnomalia + "," +valore + "," +unitaMisura + "," +data + "," +ora;


        int velocita_istantanea;//km/h
        int livello_carburante;//litri
        int consumo_carburante;//litri consumati
        String autonomia;//km rimanenti in base al livello di carburante
        boolean stato_cintura = false;
        int distanza_percorsa;//km in tempo reale
        int temperatura_motore;

        try{
            BufferedWriter fileRealTimeData = new BufferedWriter(new FileWriter(new File(getApplicationContext().getFilesDir(),fileNameData)));

            distanza_percorsa = 0;
            for(int j=70; j>0; j--){
                if(j == 68){
                    stato_cintura = true;
                }
                    for(int i = 0; i<9; i++) {
                        velocita_istantanea = x.nextInt((i + 1) * 12);
                        livello_carburante = j;
                        consumo_carburante = (70 - j);
                        autonomia = j * 10 + "." + (9 - i);
                        temperatura_motore = 80 + x.nextInt(10);
                        String anomalia = "";

                        //invio anomalie
                        if(j==67 && i==0){
                            anomalia = anomalia_1;
                        }
                        if(j==60 && i==0){
                            anomalia = anomalia_2;
                        }
                        if(j==55 && i==0){
                            anomalia = anomalia_3;
                        }
                        if(j==50 && i==0){
                            anomalia = anomalia_4;
                        }
                        if(j==45 && i==0){
                            anomalia = anomalia_5;
                        }
                        //se OBD non rileva alcuna anomalia, il campo anomalia sarà null
                        fileRealTimeData.write(
                                velocita_istantanea + "," +
                                livello_carburante + "," +
                                consumo_carburante + "," +
                                autonomia + "," +
                                stato_cintura + "," +
                                distanza_percorsa + "." + i + "," +
                                temperatura_motore +","+
                                anomalia+"\n");
                    }
                distanza_percorsa++;
            }
            fileRealTimeData.close();

        }catch (Exception e) {
        }

        /*Esempio Scrittura su file
        try{
            BufferedWriter fos = new BufferedWriter(new FileWriter(new File(getApplicationContext().getFilesDir(),fileName)));
            fos.write(x+"\n"+x+"\n");
            fos.close();
        }catch (IOException e){

        }*/


        /*Esempio lettura da file
        try{
            BufferedReader fos = new BufferedReader(new FileReader(new File(getApplicationContext().getFilesDir(),fileName)));
            String line;
            while( (line = fos.readLine()) != null){
                text.append(line);
            }
            fos.close();
        }catch (IOException e){

        }*/

    }

    private void checkBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), R.string.bluetooth_not_supported, Toast.LENGTH_SHORT).show();
        } else {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            }else{
                mBluetoothOBDServer = new BluetoothOBDServiceServer(this,mHandler);
                mBluetoothOBDServer.start();
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    //Lancia il servizio OBD
                    mBluetoothOBDServer = new BluetoothOBDServiceServer(this,mHandler);
                    mBluetoothOBDServer.start();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bluetooth_not_enabled,Toast.LENGTH_SHORT).show();
                    this.finish();
                }
        }
    }

    private void send_data_OBD(){
        //LETTURA CSV REALTIME DATA
        try{
            while(true){
                BufferedReader file = new BufferedReader(new FileReader(new File(getApplicationContext().getFilesDir(),fileNameData)));
                String line;
                while( (line = file.readLine()) != null){
                    line = line+"\n";
                    mBluetoothOBDServer.write(line.getBytes());
                    //invia i dati ogni secondo
                    TimeUnit.SECONDS.sleep(1);
                }
                file.close();
            }
        }catch (IOException e){

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * The Handler that gets information back from the BluetoothChatService
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Constants.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothOBDServiceServer.STATE_CONNECTED:
                            Toast.makeText(MainActivity.this, getString(R.string.connect_to)+ " " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                            //durante lo stato di connessione il dispositivo OBD_Server invia/scrive i dati all'app cliente
                            send_data_OBD();
                            break;
                        case BluetoothOBDServiceServer.STATE_CONNECTING:
                            Toast.makeText(MainActivity.this, getString(R.string.connecting), Toast.LENGTH_SHORT).show();
                            break;
                        case BluetoothOBDServiceServer.STATE_LISTEN:
                            break;
                        case BluetoothOBDServiceServer.STATE_NONE:
                            break;
                    }
                    break;
                case Constants.MESSAGE_WRITE:
                    break;
                case Constants.MESSAGE_READ:
                    //Non deve leggere dati, ma solo inviarli.
                    break;
                case Constants.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(Constants.DEVICE_NAME);
                    Toast.makeText(MainActivity.this, "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT).show();
                    break;
                case Constants.MESSAGE_TOAST:
                    Toast.makeText(MainActivity.this, msg.getData().getString(Constants.TOAST),Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}
