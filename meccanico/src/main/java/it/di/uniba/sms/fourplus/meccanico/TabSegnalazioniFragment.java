package it.di.uniba.sms.fourplus.meccanico;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;


public class TabSegnalazioniFragment extends Fragment
{
    public static Context activity = null, context = null;
    private View view;
    public static ListView lv;
    public static ArrayList<Anomalia> data = new ArrayList<>();
    public DatabaseReference mDatabase;
    public static ArrayList<String> clienti = new ArrayList<>();
    public static ArrayList<Cliente> clientiInviare = new ArrayList<>();
    private LinearLayout linear_click;

    public TabSegnalazioniFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tab_segnalazioni, container, false);

        activity = getActivity();
        context = getContext();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        //list view
        lv = view.findViewById(R.id.lv_Segnalazioni);

        DatabaseReference ref = mDatabase.child(getString(R.string.utenteDB));
        Query clientiMeccanicoQuery = ref.orderByChild(getString(R.string.utenteMeccanicoEmailDB)).equalTo(MainActivity.emailUtenteCollegato);
        clientiMeccanicoQuery.addValueEventListener(clientiListener);
        return view;
    }

    //listener per leggere i dati del cliente
    ValueEventListener clientiListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            clienti = new ArrayList<>();
            clientiInviare = new ArrayList<>();
            //leggo l'email dei clienti del meccanico loggato
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Cliente c = singleSnapshot.getValue(Cliente.class);
                clienti.add(c.getEmail());
                clientiInviare.add(c);
            }
            data = new ArrayList<>();
            DatabaseReference ref2 = mDatabase.child(getString(R.string.anomaliaDB));
            for(String email: clienti){
                Query anomaliaQuery = ref2.orderByChild(getString(R.string.anomaliaEmailDB)).equalTo(email);
                anomaliaQuery.addValueEventListener(anomalieClienteListener);
            }
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    ValueEventListener anomalieClienteListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Anomalia anomaliaLetta = singleSnapshot.getValue(Anomalia.class);
                //inserisci nella lista solamente se l'anomalia è stata segnalata dal cliente e se
                //il meccanico non l'ha ignorata
                if(anomaliaLetta.isAnomaliaSegnalata() && !anomaliaLetta.isAnomaliaIgnorataMeccanico()) {
                    //controllo che l'anomalia non sia gia presente nella lista da mostrare all'utente
                    if(!data.contains(anomaliaLetta)){
                        data.add(anomaliaLetta);
                    }
                }
            }
            MainActivity.unreadCount[1] = data.size();
            //ordina le segnalazioni in base alla data e l'ora dell'anomalia, attraverso il compareTo implementato nella classe Anomalia
            Collections.sort(data);
            MainActivity.setupTabIcons();
            ListSegnalazioniAdapter adapter = new ListSegnalazioniAdapter(data, context, mDatabase, clientiInviare);
            lv.setAdapter(adapter);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }



}
