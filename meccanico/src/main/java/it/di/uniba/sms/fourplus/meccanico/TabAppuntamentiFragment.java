package it.di.uniba.sms.fourplus.meccanico;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Appuntamento;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;


public class TabAppuntamentiFragment extends Fragment {

    public static ArrayList<Object> clientiInviare;
    CalendarView mCalendarView = null;
    ArrayAdapter<String> appuntamentiArrayAdapter;
    View fragment_appuntamenti;
    DatabaseReference mDatabase;
    Appuntamento appuntamento;
    ArrayList<String> listAppuntamenti;
    String currentData = new String();
    ArrayList<Cliente> clienti = new ArrayList<>();

    public TabAppuntamentiFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onActivityCreated(Bundle saveInstanceState) {
        super.onActivityCreated(saveInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        fragment_appuntamenti = inflater.inflate(R.layout.fragment_tab_appuntamenti, container, false);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mCalendarView = (CalendarView) fragment_appuntamenti.findViewById(R.id.calendarView3);
        mCalendarView.setOnDateChangeListener(mCalendarViewListener);

        //verifico se ci sono appuntamenti nella data odierna
        GregorianCalendar calendar = new GregorianCalendar();
        currentData = calendar.get(Calendar.DAY_OF_MONTH)+"-"+(calendar.get(Calendar.MONTH)+1)+"-"+calendar.get(Calendar.YEAR)+"";

        //query per capire quali sono i clienti del meccanico loggato
        DatabaseReference ref = mDatabase.child(getString(R.string.utenteDB));
        Query clientiMeccanicoQuery = ref.orderByChild(getString(R.string.utenteMeccanicoEmailDB)).equalTo(MainActivity.emailUtenteCollegato);
        clientiMeccanicoQuery.addValueEventListener(clientiListener);

        // Inflate the layout for this fragment
        return fragment_appuntamenti;
    }

    CalendarView.OnDateChangeListener mCalendarViewListener = new CalendarView.OnDateChangeListener() {
        @Override
        public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
            //il conteggio dei mesi parte da 0
            i1++;
            currentData = i2+"-"+i1+"-"+i;

            //prendo prima tutti i clienti di quel meccanico
            DatabaseReference ref = mDatabase.child(getString(R.string.utenteDB));
            Query clientiMeccanicoQuery = ref.orderByChild(getString(R.string.utenteMeccanicoEmailDB)).equalTo(MainActivity.emailUtenteCollegato);
            clientiMeccanicoQuery.addValueEventListener(clientiListener);


        }
    };

    //listener per leggere i dati del cliente
    ValueEventListener clientiListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            clienti = new ArrayList<>();
            //leggo l'email dei clienti del meccanico loggato
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Cliente c = singleSnapshot.getValue(Cliente.class);
                clienti.add(c);
            }
            //quando clicca sulla data , vai a leggere dal database gli appuntamenti relativi alla data selezionata
            listAppuntamenti = new ArrayList<>();

            // listener per leggere i dati degli appuntamenti dal database
            DatabaseReference ref = mDatabase.child(getString(R.string.appuntamentoDB));
            Query appuntamento = ref.orderByChild(getString(R.string.appuntamentoDataDB)).equalTo(currentData);
            appuntamento.addValueEventListener(appuntamentoListener);

        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };


    //listener per leggere i dati dell'appuntamento del database
    ValueEventListener appuntamentoListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            listAppuntamenti = new ArrayList<>();
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                appuntamento = singleSnapshot.getValue(Appuntamento.class);
                for(Cliente cliente: clienti){
                    if(cliente.getEmail().equals(appuntamento.getEmailCliente())){
                        String ora;
                        if(appuntamento.getOra().length() == 5){
                            ora = appuntamento.getOra();
                        }else{
                            ora = "0"+appuntamento.getOra();
                        }
                        listAppuntamenti.add(
                                getString(R.string.time_app)+" "+ora+"\n"+
                                        getString(R.string.client_app)+" "+ cliente.getNome()+" "+cliente.getCognome()+"\n"+
                                        getString(R.string.email_app)+" "+cliente.getEmail()+"\n"+
                                        getString(R.string.anom_app)+" "+appuntamento.getCodiceAnomalia()+"   "+getString(R.string.appointment_app)+" "+appuntamento.getCodiceAppuntamento()
                        );
                    }
                }
            }
            //ordino gli appuntamenti in base all'ora presente nella stringa dell'item
            Collections.sort(listAppuntamenti);
            ListView list = fragment_appuntamenti.findViewById(R.id.appuntamenti_list_view);
            appuntamentiArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.appuntamento,listAppuntamenti);
            list.setAdapter(appuntamentiArrayAdapter);
            list.setOnItemClickListener(mAppuntamentoClickListener);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    AdapterView.OnItemClickListener mAppuntamentoClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> av, View v, int arg2, long arg3) {
            Intent appuntamentoDetail = new Intent(getActivity(),AppuntamentoDetailActivity.class);


            String itemList = ((TextView)v).getText().toString();
            String[] itemList_content = itemList.split("\n");
            String id_anomalia = itemList_content[3].split(" ")[1];
            String id_appuntamento = itemList_content[3].split("   ")[1].split(" ")[1];
            String email_cliente = itemList_content[2].split(" ")[1];

            appuntamentoDetail.putExtra(getString(R.string.keyAnomaly),id_anomalia);
            appuntamentoDetail.putExtra(getString(R.string.keyAppointment),id_appuntamento);
            appuntamentoDetail.putExtra(getString(R.string.keyEmailMec),email_cliente);
            appuntamentoDetail.putExtra(getString(R.string.keyDateApp),currentData);
            startActivity(appuntamentoDetail);
        }
    };

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
    }

}
