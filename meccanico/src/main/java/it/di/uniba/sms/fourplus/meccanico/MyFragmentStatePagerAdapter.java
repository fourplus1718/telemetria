package it.di.uniba.sms.fourplus.meccanico;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

//MyFragmentStatePagerAdapter definisce i metodi ereditati dalla classe astratta FragmentStatePagerAdapter in modo che possa
//restituire il corretto Fragment in base al tab selezionato

//la classe FragmentStatePagerAdapter è una implementazione di PagerAdapter che usa un fragment per gestire ogni pagina

public class MyFragmentStatePagerAdapter extends FragmentStatePagerAdapter
{
    int mNumOfTabs;

    public MyFragmentStatePagerAdapter(FragmentManager fm, int numOfTabs)
    {
        super(fm);
        this.mNumOfTabs=numOfTabs;
    }

    @Override
    public int getCount()
    {
        return mNumOfTabs;
    }


    @Override
    public Fragment getItem(int position)
    {
        switch(position){
            case 0:
                return new TabAppuntamentiFragment();
            case 1:
                return new TabSegnalazioniFragment();
            default:
                return null;

        }
    }
}
