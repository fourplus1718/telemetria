package it.di.uniba.sms.fourplus.meccanico;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Random;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Appuntamento;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;


public class SetDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    DatabaseReference mDatabase;
    Appuntamento appuntamento;
    String newDate = new String();
    TextView info;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, dayOfMonth);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        month++;
        newDate = dayOfMonth+"-"+month+"-"+year;
        mDatabase = FirebaseDatabase.getInstance().getReference();

        String email_meccanico = MainActivity.emailUtenteCollegato;
        info = getActivity().findViewById(R.id.textView_valoreEmailCliente);
        String email_cliente = info.getText().toString();
        info = getActivity().findViewById(R.id.textView_valoreCodiceAnomalia);
        String codice_anomalia = info.getText().toString();

        Random cod = new Random();
        appuntamento = new Appuntamento(newDate,"15:00",email_meccanico,email_cliente,codice_anomalia,cod.nextInt(100000)+"");
        writeNewAppuntamento(appuntamento);
        Button bt = getActivity().findViewById(R.id.setDataAppuntamento);
        bt.setEnabled(false);

        //query per reimpostare la lista delle segnalazioni con i dati aggiornati
        DatabaseReference ref = mDatabase.child(getString(R.string.anomaliaDB));
        Query anomaliaQuery = ref.orderByChild(getString(R.string.anomaliaCodiceDB)).equalTo(codice_anomalia);
        anomaliaQuery.addValueEventListener(anomalyListener);

        Toast.makeText(getActivity(),R.string.newDateOk, Toast.LENGTH_SHORT).show();
    }

    ValueEventListener anomalyListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            Anomalia anomaliaLetta=null;
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                anomaliaLetta = singleSnapshot.getValue(Anomalia.class);
            }
            anomaliaLetta.setAnomaliaIgnorataMeccanico(true);
            mDatabase.child(getString(R.string.anomaliaDB)).child(anomaliaLetta.getCodice()).setValue(anomaliaLetta);

            DatabaseReference ref = mDatabase.child("utente");
            Query clientiMeccanicoQuery = ref.orderByChild("emailMeccanico").equalTo(MainActivity.emailUtenteCollegato);
            clientiMeccanicoQuery.addValueEventListener(clientiListener);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    private void writeNewAppuntamento(Appuntamento appuntamento) {
        mDatabase.child(getString(R.string.appuntamentoDB)).child(appuntamento.getCodiceAppuntamento()).setValue(appuntamento);
    }

    //listener per leggere i dati del cliente
    ValueEventListener clientiListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            TabSegnalazioniFragment.clienti = new ArrayList<>();
            TabSegnalazioniFragment.clientiInviare = new ArrayList<>();
            //leggo l'email dei clienti del meccanico loggato
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Cliente c = singleSnapshot.getValue(Cliente.class);
                TabSegnalazioniFragment.clienti.add(c.getEmail());
                TabSegnalazioniFragment.clientiInviare.add(c);
            }
            TabSegnalazioniFragment.data = new ArrayList<>();
            DatabaseReference ref2 = mDatabase.child("anomalia"); //todo commenta nunzio
            for(String email: TabSegnalazioniFragment.clienti){
                Query anomaliaQuery = ref2.orderByChild("emailCliente").equalTo(email);
                anomaliaQuery.addValueEventListener(anomalieClienteListener);
            }
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    ValueEventListener anomalieClienteListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Anomalia anomaliaLetta = singleSnapshot.getValue(Anomalia.class);
                //inserisci nella lista solamente se l'anomalia è stata segnalata dal cliente e se
                //il meccanico non l'ha ignorata
                if(anomaliaLetta.isAnomaliaSegnalata() && !anomaliaLetta.isAnomaliaIgnorataMeccanico()) {
                    //controllo che l'anomalia non sia gia presente nella lista da mostrare all'utente
                    if(!TabSegnalazioniFragment.data.contains(anomaliaLetta)){
                        TabSegnalazioniFragment.data.add(anomaliaLetta);
                    }
                }
            }
            MainActivity.unreadCount[1] = TabSegnalazioniFragment.data.size();
            //ordina le segnalazioni in base alla data e l'ora dell'anomalia, attraverso il compareTo implementato nella classe Anomalia
            Collections.sort(TabSegnalazioniFragment.data);
            MainActivity.setupTabIcons();
            ListSegnalazioniAdapter adapter = new ListSegnalazioniAdapter(TabSegnalazioniFragment.data, TabSegnalazioniFragment.context, mDatabase, TabSegnalazioniFragment.clientiInviare);
            TabSegnalazioniFragment.lv.setAdapter(adapter);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };


}