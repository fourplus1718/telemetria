package it.di.uniba.sms.fourplus.meccanico;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Meccanico;

import static it.di.uniba.sms.fourplus.meccanico.SettingsActivity.mAuth;

public class MainActivity extends AppCompatActivity {

    //ViewPager permette lo swipe delle pagine sullo schermo (vedi anche in XML)
    static ViewPager viewPager;

    //tab layout è il layout contenente i due tab
    //public TabLayout tabs;

    DatabaseReference mDatabase;
    Meccanico meccanico = new Meccanico();
    public static String emailUtenteCollegato;

    static Activity activity;
    static Context context;
    static String[] tabTitle;
    public static int[] unreadCount = {0, 0};
    private SectionPageAdapter sectionPageAdapter;
    public static TabLayout tabLayout;
    static LayoutInflater li;
    View title;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        context = this;
        activity = this;

        li = LayoutInflater.from(context); //per poter rendere prepareTabView static

        //toolbar della pagina del Meccanico
        toolbar = (Toolbar) findViewById(R.id.Scheda_Toolbar);
        toolbar.setTitle("  "+getString(R.string.app_name));
        toolbar.setLogo(R.mipmap.icona_meccanico);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setSubtitleTextColor(getResources().getColor(R.color.colorWhiteDarker));
        setSupportActionBar(toolbar);

        //emailUtenteCollegato = "antonio@gmail.com";
        emailUtenteCollegato = getIntent().getStringExtra(getString(R.string.keyEmailMec));
        tabTitle = new String[]{getResources().getString(R.string.tab_appuntamenti),getResources().getString(R.string.tab_segnalazioni)};
        sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.pager);
        //viewPager.setOffscreenPageLimit(2);

        tabLayout = findViewById(R.id.TabMeccanico);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabTextColors(getResources().getColor(R.color.colorWhiteDarker),getResources().getColor(R.color.colorWhite));

        //leggi informazioni meccanico loggato
        DatabaseReference ref = mDatabase.child(getString(R.string.meccanicoDB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.meccanicoEmailDB)).equalTo(emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(datiMeccanicoListener);

        setupViewPager(viewPager);
    }

    //questa funzione permette di settare i badge e titoli delle tab
    public static void setupTabIcons() {
        tabLayout.setupWithViewPager(viewPager); //aggiorna la tab ogni volta che viene chiamata questa funzione
        for(int i=0;i<tabLayout.getTabCount();i++) {
            tabLayout.getTabAt(i).setCustomView(prepareTabView(i));
        }
    }

    //questa funzione permette di impostare le pagine della tab
    private void setupViewPager(ViewPager vg) {
        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new TabAppuntamentiFragment(), getResources().getString(R.string.tab_appuntamenti));
        adapter.addFragment(new TabSegnalazioniFragment(), getResources().getString(R.string.tab_segnalazioni));
        vg.setAdapter(adapter);
    }

    ValueEventListener datiMeccanicoListener = new ValueEventListener(){

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Meccanico meccanico = singleSnapshot.getValue(Meccanico.class);
                toolbar.setSubtitle("  "+meccanico.getNome() + " " + meccanico.getCognome());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    //funzione che setta il badge e il titolo della singola tab
    private static View prepareTabView(int pos) {
        View view = li.inflate(R.layout.custom_tab,null);
        TextView tv_title = view.findViewById(R.id.tv_title);
        TextView tv_count = view.findViewById(R.id.tv_count);
        tv_title.setText(tabTitle[pos]);
        if(unreadCount[pos]>0)
        {
            tv_count.setVisibility(View.VISIBLE);
            tv_count.setText(""+unreadCount[pos]);
        }
        else
            tv_count.setVisibility(View.GONE);

        return view;
    }

    TabLayout.OnTabSelectedListener tabListener =  new TabLayout.OnTabSelectedListener(){

        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            //imposta la pagina corretta quando viene selezionato il corrispondente tab
            //evidentemente, verrà invocato getItem (forse non sempre)
            viewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.meccanico_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.impostazioni_meccanico:
                startActivity(new Intent(MainActivity.this,SettingsActivity.class));
                return true;
            case R.id.logout:
                mAuth = FirebaseAuth.getInstance();
                mAuth.signOut();
                Intent goLogin = new Intent(activity, LoginActivityMeccanico.class);
                startActivity(goLogin);
                activity.finish();
                MainActivity.activity.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

}