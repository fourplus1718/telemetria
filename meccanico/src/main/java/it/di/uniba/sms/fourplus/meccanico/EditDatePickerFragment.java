package it.di.uniba.sms.fourplus.meccanico;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Appuntamento;


public class EditDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    TextView textViewInfoDataAppuntamento;
    DatabaseReference mDatabase;
    Appuntamento appuntamento = new Appuntamento();
    String newDate = new String();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, dayOfMonth);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        //textViewInfoDataAppuntamento = (TextView) getActivity().findViewById(R.id.infoDataAppuntamento);
        month++;
        //textViewInfoDataAppuntamento.setText("Data: "+dayOfMonth+"-"+month+"-"+year);
        newDate = dayOfMonth+"-"+month+"-"+year;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        String codiceAppuntamento = ((AppuntamentoDetailActivity)getActivity()).getCodiceAppunatamento();
        writeNewAppuntamento(codiceAppuntamento);
        Toast.makeText(getActivity(),R.string.newDateOk, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    //aggiornamento data nell'appuntamento
    public Map<String, Object> toMap(String data) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("data", data);
        return result;
    }

    private void writeNewAppuntamento(String codiceAppuntamento) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> updateData = toMap(newDate);
        mDatabase.child(getString(R.string.appuntamentoDB)).child(codiceAppuntamento).updateChildren(updateData);
    }

}