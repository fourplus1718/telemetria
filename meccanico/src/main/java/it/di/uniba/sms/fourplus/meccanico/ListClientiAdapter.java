package it.di.uniba.sms.fourplus.meccanico;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;

public class ListClientiAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Cliente> list;
    private Context context;
    DatabaseReference mDatabase;
    TextView tv_cliente_email;
    Cliente cli_rem;

    public ListClientiAdapter(ArrayList<Cliente> list, Context context, DatabaseReference mDatabase) {
        this.list = list;
        this.context = context;
        this.mDatabase = mDatabase;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_list_clients, null);
        }

        Cliente cliente;
        cliente = list.get(position);

        TextView tv_nome_cognome_cli = view.findViewById(R.id.nome_cognome_cli);
        tv_nome_cognome_cli.setText(context.getString(R.string.cli)+": "+cliente.getNome()+" "+cliente.getCognome());

        tv_cliente_email = view.findViewById(R.id.email_cli);
        tv_cliente_email.setText(context.getString(R.string.clientEmail)+": "+cliente.getEmail()+"");

        TextView tv_tel_cli = view.findViewById(R.id.telefono_cli);
        tv_tel_cli.setText(context.getString(R.string.clientTel)+": "+cliente.getTelefono()+"");

        final Button btn_remove_client = view.findViewById(R.id.elimina_cli);

        btn_remove_client.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(final View v) {
                Animation anim = (AnimationUtils.loadAnimation(context, R.anim.slide_out)); //todo: oppure fade out
                anim.setDuration(500);
                parent.getChildAt(position).startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        //una volta finita l'animazione, elimina la riga
                        updateRemoved(position);
                        cli_rem = list.get(position);
                        list.remove(position);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                        undoRemoved(cli_rem, v, list);
                    }
                });
            }
        });

        return view;
    }

    //serve per eliminare il cliente
    private void updateRemoved(int pos) {
        Cliente client = new Cliente(list.get(pos).getNome(), list.get(pos).getCognome(), list.get(pos).getEmail(),
                list.get(pos).getPassword(), list.get(pos).getTelefono(), list.get(pos).getModelloVeicolo(),list.get(pos).getEmailMeccanico());
        client.setEmailMeccanico("");
        mDatabase.child(context.getString(R.string.utenteDB)).child(list.get(pos).getTelefono()).setValue(client);
    }

    //attraverso uno snackbar, questa funzione permette l'annullamento dell'eliminazione del cliente
    private void undoRemoved(final Cliente cli, View view, final ArrayList<Cliente> list) {
        Snackbar.make(view, context.getString(R.string.anomalyIgnored), Snackbar.LENGTH_LONG)
                .setActionTextColor(view.getResources().getColor(R.color.colorAccent))
                .setAction(context.getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cli.setEmailMeccanico(MainActivity.emailUtenteCollegato);
                        mDatabase.child(context.getString(R.string.utenteDB)).child(cli.getTelefono()).setValue(cli);
                        list.add(cli);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                    }
                })
                .show();
    }
}
