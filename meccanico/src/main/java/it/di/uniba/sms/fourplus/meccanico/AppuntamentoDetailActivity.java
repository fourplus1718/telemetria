package it.di.uniba.sms.fourplus.meccanico;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;

public class AppuntamentoDetailActivity extends AppCompatActivity
{

    TextView textView_valore_nome_cliente;
    TextView textView_valore_cognome_cliente;
    TextView textView_valore_email_cliente;
    TextView textView_valore_telefono_cliente;
    TextView textView_valore_modello_veicolo;
    TextView textView_valore_val_anomalia;
    TextView textView_valore_codice_anomalia;
    TextView textView_valore_tipo_anomalia;
    Button changeDate;
    Button changeTime;
    DatabaseReference mDatabase;
    Cliente cliente = new Cliente();
    Anomalia anomalia = new Anomalia();
    String codiceAnomalia;
    String codiceAppuntamento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appuntamento_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        textView_valore_nome_cliente = (TextView) findViewById(R.id.textView_valore_nome_cliente);
        textView_valore_cognome_cliente = (TextView) findViewById(R.id.textView_valore_cognome_cliente);
        textView_valore_email_cliente = (TextView) findViewById(R.id.textView_valore_email_cliente);
        textView_valore_telefono_cliente = (TextView) findViewById(R.id.textView_valore_telefono_cliente);
        textView_valore_modello_veicolo = (TextView) findViewById(R.id.textView_valore_modello_veicolo);
        textView_valore_val_anomalia = (TextView) findViewById(R.id.textView_valore_val_anomalia);
        textView_valore_codice_anomalia = (TextView) findViewById(R.id.textView_valore_codice_anomalia);
        textView_valore_tipo_anomalia = (TextView) findViewById(R.id.textView_valore_tipo_anomalia);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send_mess = new Intent(Intent.ACTION_SENDTO);
                String mess = getResources().getString(R.string.sendMessage);
                send_mess.putExtra("sms_body",mess);
                send_mess.setData(Uri.parse("smsto:"+cliente.getTelefono()));
                startActivity(send_mess);
            }
        });

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        codiceAppuntamento = getIntent().getExtras().getString(getString(R.string.keyAppointment));
        String codice_anomalia = getIntent().getExtras().getString(getString(R.string.keyAnomaly));
        String email_cliente = getIntent().getExtras().getString(getString(R.string.keyEmailMec));

        // listener per leggere i dettagli dal database
        DatabaseReference ref_cliente = mDatabase.child(getString(R.string.utenteDB));
        Query clienteQuery = ref_cliente.orderByChild(getString(R.string.utenteEmailDB)).equalTo(email_cliente);
        clienteQuery.addValueEventListener(clienteListener);

        codiceAnomalia = codice_anomalia;
        DatabaseReference ref_anomalia = mDatabase.child(getString(R.string.anomaliaDB));
        Query anomaliaQuery = ref_anomalia.orderByChild(getString(R.string.anomaliaCodiceDB)).equalTo(codice_anomalia);
        anomaliaQuery.addValueEventListener(anomaliaListener);

        changeDate = (Button) findViewById(R.id.modificaDataAppuntamento);
        changeDate.setOnClickListener(showDatePickerDialog);

        changeTime = (Button) findViewById(R.id.modificaOraAppuntamento);
        changeTime.setOnClickListener(showTimePickerDialog);
    }

    //listener per leggere i dati del cliente  del database
    ValueEventListener clienteListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                cliente = singleSnapshot.getValue(Cliente.class);
            }
            textView_valore_nome_cliente.setText(cliente.getNome());
            textView_valore_cognome_cliente.setText(cliente.getCognome());
            textView_valore_email_cliente.setText(cliente.getEmail());
            textView_valore_telefono_cliente.setText(cliente.getTelefono());
            textView_valore_modello_veicolo.setText(cliente.getModelloVeicolo());
            //textViewCliente.setText(cliente.toString());
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    //listener per leggere i dati dell'anomalia  del database
    ValueEventListener anomaliaListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                anomalia = singleSnapshot.getValue(Anomalia.class);
            }
            textView_valore_val_anomalia.setText(anomalia.getValore()+" "+anomalia.getUnitàMisura());
            textView_valore_codice_anomalia.setText(anomalia.getCodice());
            textView_valore_tipo_anomalia.setText(anomalia.getTipoAnomalia());

            //textViewAnomalia.setText(anomalia.toString());
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //per tornare nella activity precedente nel backstack
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener showDatePickerDialog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment newFragment = new EditDatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
        }
    };

    View.OnClickListener showTimePickerDialog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment newFragment = new EditTimePickerFragment();
            newFragment.show(getSupportFragmentManager(), "timePicker");
        }
    };

    //il codice che passo al date picker per accedere all'istanza dell'appuntamento sul database
    public String getCodiceAppunatamento(){
        return this.codiceAppuntamento;
    }
}
