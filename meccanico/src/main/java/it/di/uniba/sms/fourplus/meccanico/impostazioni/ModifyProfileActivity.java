package it.di.uniba.sms.fourplus.meccanico.impostazioni;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import it.di.uniba.sms.fourplus.meccanico.MainActivity;
import it.di.uniba.sms.fourplus.meccanico.R;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Meccanico;

public class ModifyProfileActivity extends AppCompatActivity {

    EditText et_nome;
    EditText et_cognome;
    EditText et_email;
    EditText et_password;
    EditText et_telefono;
    EditText et_indirizzoOfficina;
    Button btn_save;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        et_nome = findViewById(R.id.et_nome_meccanico);
        et_cognome = findViewById(R.id.et_cognome_meccanico);
        et_email = findViewById(R.id.et_email_meccanico);
        et_password = findViewById(R.id.et_password_meccanico);
        et_telefono = findViewById(R.id.et_telefono_meccanico);
        et_indirizzoOfficina = findViewById(R.id.et_officina_meccanico);
        btn_save = findViewById(R.id.btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aggiornamento di un record nella tabella users
                updateUser(et_nome.getText().toString(), et_cognome.getText().toString(), et_email.getText().toString(),
                        et_password.getText().toString(), et_telefono.getText().toString(), et_indirizzoOfficina.getText().toString());
                Toast.makeText(getApplicationContext(), getString(R.string.modify_successful), Toast.LENGTH_SHORT).show();
                finish();

            }
        });

        //query per visualizzare i dati già presenti nel db corrispondenti al cliente loggato
        DatabaseReference ref = mDatabase.child(getString(R.string.meccanicoDB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.meccanicoEmailDB)).equalTo(MainActivity.emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(datiUtenteListener);
    }

    ValueEventListener datiUtenteListener = new ValueEventListener(){

        //controlla se è stato modificato un campo del profilo utente
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Meccanico meccanico = singleSnapshot.getValue(Meccanico.class);
                et_nome.setText(meccanico.getNome());
                et_cognome.setText(meccanico.getCognome());
                et_email.setText(meccanico.getEmail());
                et_password.setText(meccanico.getPassword());
                et_telefono.setText(meccanico.getTelefono());
                et_indirizzoOfficina.setText(meccanico.getIndirizzo());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    private void updateUser(String nome, String cognome, String email, String password, String telefono,
                              String indirizzoOfficina) {
        Meccanico user = new Meccanico(nome, cognome, email, password, telefono, indirizzoOfficina);

        mDatabase.child(getString(R.string.meccanicoDB)).child(telefono).setValue(user);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
