package it.di.uniba.sms.fourplus.meccanico;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;

public class ListSegnalazioniAdapter extends BaseAdapter implements ListAdapter {
    private ArrayList<Anomalia> list;
    private Context context;
    DatabaseReference mDatabase;
    Anomalia anomUndo;
    TextView tv_cliente_anom;
    ArrayList<Cliente> clienti;

    public ListSegnalazioniAdapter(ArrayList<Anomalia> list, Context context, DatabaseReference mDatabase, ArrayList<Cliente> clienti) {
        this.list = list;
        this.context = context;
        this.mDatabase = mDatabase;
        this.clienti = clienti;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.row_list_anomalies, null);
        }

        Anomalia anomalia;

        anomalia = list.get(position);

        TextView tv_nome_anom = view.findViewById(R.id.tv_nome_anom);
        tv_nome_anom.setText(anomalia.getTipoAnomalia());

        tv_cliente_anom = view.findViewById(R.id.tv_cliente_anom);
        //Query clientiMeccanicoQuery = ref.orderByChild("emailMeccanico").equalTo(MainActivity.emailUtenteCollegato);

        for(Cliente c: clienti){
            if(c.getEmail().equals(anomalia.getEmailCliente())){
                tv_cliente_anom.setText(""+context.getString(R.string.anomalyClient) +": "+ c.getCognome()+" "+c.getNome());
            }
        }

        //tv_cliente_anom.setText(""+context.getString(R.string.anomalyClient) +": "+ anomalia.getEmailCliente());

        TextView tv_data_anom = view.findViewById(R.id.tv_data_anom);
        tv_data_anom.setText(""+context.getString(R.string.anomalyDate) + ": "+anomalia.getDataAnomalia()+"   "+anomalia.getoraAnomalia());

        /**
        DatabaseReference ref = mDatabase.child("utente");
        Query clientiQuery = ref.orderByChild("email").equalTo(list.get(position).getEmailCliente());
        clientiQuery.addValueEventListener(clientiListener);*/

        final Button btn_dettagli = view.findViewById(R.id.btn_dettagli);
        final Button btn_ignora = view.findViewById(R.id.btn_ignora);
        //linear_appointment.setVisibility(View.GONE); lo tolgo perchè è impostato nel layout

        btn_dettagli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Anomalia a = (Anomalia) getItem(position);
                String codice = a.getCodice();
                Intent gotoDettaglio = new Intent(context,SegnalazioneDetailActivity.class);
                gotoDettaglio.putExtra(context.getString(R.string.keyCodAnomaly),codice);
                context.startActivity(gotoDettaglio);
            }
        });

        btn_ignora.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(final View v) {
                Animation anim = (AnimationUtils.loadAnimation(context, R.anim.slide_out)); //todo: oppure fade out
                anim.setDuration(500);
                parent.getChildAt(position).startAnimation(anim);
                anim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation arg0) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation arg0) {
                    }

                    @Override
                    public void onAnimationEnd(Animation arg0) {
                        //una volta finita l'animazione, elimina la riga
                        updateFlagIgnored(position);
                        anomUndo = list.get(position);
                        list.remove(position);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                        undoIgnored(anomUndo, v, list);
                    }
                });
            }
        });

        return view;
    }

    //serve per aggiornare le segnalazioni e dire che è stata ignorata
    private void updateFlagIgnored(int pos) {
        Anomalia anom = new Anomalia(list.get(pos).getCodice(), list.get(pos).getTipoAnomalia(), list.get(pos).getValore(),
                list.get(pos).getUnitàMisura(), list.get(pos).getEmailCliente(), list.get(pos).getDataAnomalia(),list.get(pos).getoraAnomalia());
        anom.setAnomaliaIgnorataMeccanico(true);
        anom.setAnomaliaIgnorata(list.get(pos).isAnomaliaIgnorata());
        mDatabase.child(context.getString(R.string.anomaliaDB)).child(list.get(pos).getCodice()).setValue(anom);
    }

    //attraverso uno snackbar, questa funzione permette l'annullamento dell'ignoramento segnalazioni
    private void undoIgnored(final Anomalia anomalia, View view, final ArrayList<Anomalia> list) {
        Snackbar.make(view, context.getString(R.string.anomalyIgnored), Snackbar.LENGTH_LONG)
                .setActionTextColor(view.getResources().getColor(R.color.colorAccent))
                .setAction(context.getString(R.string.cancel), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        anomalia.setAnomaliaIgnorata(false);
                        //todo: animazione entrante
                        mDatabase.child(context.getString(R.string.anomaliaDB)).child(anomalia.getCodice()).setValue(anomalia);
                        list.add(anomalia);
                        notifyDataSetChanged(); //serve per aggiornare la listview ai nuovi cambiamenti
                    }
                })
                .show();
    }
}
