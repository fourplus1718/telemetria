package it.di.uniba.sms.fourplus.meccanico.classiDB;

import android.support.annotation.NonNull;

import java.util.Date;
import java.util.Objects;

public class Anomalia implements Comparable{
    private String codice;
    private String tipoAnomalia;
    private String valore;
    private String unitàMisura;
    private String emailCliente;
    private boolean anomaliaIgnorata;
    private boolean anomaliaSegnalata;
    private String dataAnomalia;
    private String oraAnomalia;
    private boolean anomaliaIgnorataMeccanico=false;

    public Anomalia() {

    }

    public Anomalia(String codice, String tipoAnomalia, String valore, String unitàMisura,
                    String emailCliente, String dataAnomalia,String oraAnomalia) {
        this.codice = codice;
        this.tipoAnomalia = tipoAnomalia;
        this.valore = valore;
        this.unitàMisura = unitàMisura;
        this.emailCliente = emailCliente;
        this.anomaliaIgnorata = false;
        this.anomaliaSegnalata = false;
        this.dataAnomalia = dataAnomalia;
        this.anomaliaIgnorataMeccanico = false;
        this.oraAnomalia = oraAnomalia;

    }

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getoraAnomalia() {
        return oraAnomalia;
    }

    public void setoraAnomalia(String oraAnomalia) {
        this.oraAnomalia = oraAnomalia;
    }

    public String getTipoAnomalia() {
        return tipoAnomalia;
    }

    public void setTipoAnomalia(String tipoAnomalia) {
        this.tipoAnomalia = tipoAnomalia;
    }

    public String getValore() {
        return valore;
    }

    public void setValore(String valore) {
        this.valore = valore;
    }

    public String getUnitàMisura() {
        return unitàMisura;
    }

    public void setUnitàMisura(String unitàMisura) {
        this.unitàMisura = unitàMisura;
    }

    public String getEmailCliente() {
        return emailCliente;
    }

    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    public boolean isAnomaliaIgnorata() {
        return anomaliaIgnorata;
    }

    public void setAnomaliaIgnorata(boolean anomaliaIgnorata) {
        this.anomaliaIgnorata = anomaliaIgnorata;
    }

    public boolean isAnomaliaSegnalata() {
        return anomaliaSegnalata;
    }

    public void setAnomaliaIgnorataMeccanico(boolean anomaliaIgnorataMeccanico) {
        this.anomaliaIgnorataMeccanico = anomaliaIgnorataMeccanico;
    }

    public boolean isAnomaliaIgnorataMeccanico() {
        return anomaliaIgnorataMeccanico;
    }

    
    public void setAnomaliaSegnalata(boolean anomaliaSegnalata) {
        this.anomaliaSegnalata = anomaliaSegnalata;
    }

    public String getDataAnomalia() {
        return dataAnomalia;
    }

    public void setDataAnomalia(String dataAnomalia) {
        this.dataAnomalia = dataAnomalia;
    }

    public String toString(){
        return "Codice anomalia: "+getCodice()+"\n"+
                "Tipo anomalia: "+getTipoAnomalia()+"\n"+
                "Valore anomalia: "+getValore()+" "+getUnitàMisura();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Anomalia anomalia = (Anomalia) o;
        return Objects.equals(codice, anomalia.codice) &&
                Objects.equals(emailCliente, anomalia.emailCliente);
    }

    @Override
    public int hashCode() {

        return Objects.hash(codice, emailCliente);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Anomalia anomalia = (Anomalia)o;
        if((this.dataAnomalia.compareTo(anomalia.getDataAnomalia()) == 1 || this.dataAnomalia.compareTo(anomalia.getDataAnomalia()) == 0)
                && this.oraAnomalia.compareTo(anomalia.getoraAnomalia()) == 1){
            return -1;
        }else if((this.dataAnomalia.compareTo(anomalia.getDataAnomalia()) == -1 || this.dataAnomalia.compareTo(anomalia.getDataAnomalia()) == 0)
            && this.oraAnomalia.compareTo(anomalia.getoraAnomalia()) == -1){
            return 1;
        }
        return 0;
    }
}
