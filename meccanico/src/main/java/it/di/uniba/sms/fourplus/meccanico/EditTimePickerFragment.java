package it.di.uniba.sms.fourplus.meccanico;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Appuntamento;


public class EditTimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    TextView textViewInfoOraAppuntamento;
    Appuntamento appuntamento = new Appuntamento();
    DatabaseReference mDatabase;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hour, minute,true);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        //textViewInfoOraAppuntamento = (TextView) getActivity().findViewById(R.id.infoOraAppuntamento);
        String minutes = "";
        if(i1 < 10){
            minutes = "0"+i1;
        }else{
            minutes = i1+"";
        }
        String newTime = i+":"+minutes;
        //textViewInfoOraAppuntamento.setText("Ora: "+newTime);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        DatabaseReference ref = mDatabase.child(getString(R.string.appuntamentoDB));
        String codiceAppuntamento = ((AppuntamentoDetailActivity)getActivity()).getCodiceAppunatamento();
        writeNewAppuntamento(codiceAppuntamento,newTime);
        Toast.makeText(getActivity(),R.string.newTimeOk, Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }

    //aggiornamento ora nell'appuntamento
    public Map<String, Object> toMap(String ora) {
        HashMap<String, Object> result = new HashMap<>();
        result.put(getString(R.string.appuntamentoOraDB), ora);
        return result;
    }

    private void writeNewAppuntamento(String codiceAppuntamento,String newTime) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Map<String, Object> updateOra = toMap(newTime);
        mDatabase.child(getString(R.string.appuntamentoDB)).child(codiceAppuntamento).updateChildren(updateOra);
    }
}