package it.di.uniba.sms.fourplus.meccanico;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Random;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Appuntamento;


public class SetTimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    Appuntamento appuntamento;
    DatabaseReference mDatabase;
    String newTime = new String();
    TextView info;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        Button bt = getActivity().findViewById(R.id.setOraAppuntamento);
        bt.setEnabled(true);
        return new TimePickerDialog(getActivity(), this, hour, minute,true);
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {

        String minutes = "";
        if(i1 < 10){
            minutes = "0"+i1;
        }else{
            minutes = i1+"";
        }
        newTime = i+":"+minutes;

        mDatabase = FirebaseDatabase.getInstance().getReference();

        info = getActivity().findViewById(R.id.textView_valoreEmailCliente);
        String email_cliente = info.getText().toString();
        info = getActivity().findViewById(R.id.textView_valoreCodiceAnomalia);
        String codice_anomalia = info.getText().toString();

        DatabaseReference ref = mDatabase.child("appuntamento");
        Query appuntamento = ref.orderByChild("codiceAnomalia").equalTo(codice_anomalia);
        appuntamento.addValueEventListener(appuntamentoListener);
        Button bt = getActivity().findViewById(R.id.setOraAppuntamento);
        bt.setEnabled(false);
        Toast.makeText(getActivity(),R.string.newTimeOk, Toast.LENGTH_SHORT).show();
    }

    ValueEventListener appuntamentoListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                appuntamento = singleSnapshot.getValue(Appuntamento.class);
            }
            appuntamento.setOra(newTime);
            writeNewAppuntamento(appuntamento);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    private void writeNewAppuntamento(Appuntamento appuntamento) {
        mDatabase.child("appuntamento").child(appuntamento.getCodiceAppuntamento()).setValue(appuntamento);
    }
}