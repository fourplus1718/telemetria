package it.di.uniba.sms.fourplus.meccanico;

import android.app.ListActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;

public class ListClientiActivity extends AppCompatActivity {

    DatabaseReference mDatabase;
    ArrayList<Cliente> clientiInviare;
    ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_clienti);

        lv = findViewById(R.id.list_clienti);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        toolbar.setTitle(getString(R.string.list_cli));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        DatabaseReference ref = mDatabase.child(getString(R.string.utenteDB));
        Query clientiMeccanicoQuery = ref.orderByChild(getString(R.string.utenteMeccanicoEmailDB)).equalTo(MainActivity.emailUtenteCollegato);
        clientiMeccanicoQuery.addValueEventListener(clientiListener);
    }

    //listener per leggere i dati del cliente
    ValueEventListener clientiListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            clientiInviare = new ArrayList<>();
            //leggo l'email dei clienti del meccanico loggato
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Cliente c = singleSnapshot.getValue(Cliente.class);
                clientiInviare.add(c);
            }
            ListClientiAdapter adapter = new ListClientiAdapter(clientiInviare, getApplicationContext(), mDatabase);
            lv.setAdapter(adapter);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //per tornare nella activity precedente nel backstack
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
