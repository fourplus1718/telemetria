package it.di.uniba.sms.fourplus.meccanico;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Collections;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Meccanico;
import it.di.uniba.sms.fourplus.meccanico.impostazioni.ModifyProfileActivity;


/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 */
public class SettingsActivity extends AppCompatActivity {

    //private static final String TAG = SettingsActivity.class.getSimpleName();
    TextView text_nome_cliente;
    TextView text_email_cliente;
    ImageView img_meccanico;
    static FirebaseAuth mAuth;
    static Activity activity;
    static Context context;



    public DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dati_meccanico_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activity = this;
        context = this;

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        text_email_cliente = findViewById(R.id.nome_meccanico);
        text_nome_cliente = findViewById(R.id.email_meccanico);
        img_meccanico = findViewById(R.id.img_meccanico);

        text_email_cliente.setText(MainActivity.emailUtenteCollegato);
        img_meccanico.setImageResource(R.drawable.meccanico);

        DatabaseReference ref = mDatabase.child(getString(R.string.meccanicoDB));
        Query datiUtenteQuery = ref.orderByChild(getString(R.string.meccanicoEmailDB)).equalTo(MainActivity.emailUtenteCollegato);
        datiUtenteQuery.addListenerForSingleValueEvent(datiMeccanicoListener);

        // load settings fragment
        getFragmentManager().beginTransaction().replace(R.id.content_frame, new MainPreferenceFragment()).commit();
    }

    ValueEventListener datiMeccanicoListener = new ValueEventListener(){

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                Meccanico meccanico = singleSnapshot.getValue(Meccanico.class);
                text_nome_cliente.setText(meccanico.getNome() + " " + meccanico.getCognome());
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    public static class MainPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings_main);

            Preference btn_logout = findPreference(getString(R.string.key_logout));
            btn_logout.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    mAuth.signOut();
                    Intent goLogin = new Intent(activity, LoginActivityMeccanico.class);
                    startActivity(goLogin);
                    activity.finish();
                    MainActivity.activity.finish();
                    Toast.makeText(context, getString(R.string.logout_done), Toast.LENGTH_SHORT).show();
                    return true;
                }
            });

            Preference btn_modify_profile = findPreference(getString(R.string.key_modify_profile));
            btn_modify_profile.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent goModify = new Intent(activity, ModifyProfileActivity.class);
                    startActivity(goModify);
                    return true;
                }
            });

            Preference btn_list_clients = findPreference(getString(R.string.key_list_clients));
            btn_list_clients.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    Intent goCliList = new Intent(activity, ListClientiActivity.class);
                    startActivity(goCliList);
                    return true;
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
