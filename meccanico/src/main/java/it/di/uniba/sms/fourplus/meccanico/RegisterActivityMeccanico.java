package it.di.uniba.sms.fourplus.meccanico;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Meccanico;

public class RegisterActivityMeccanico extends AppCompatActivity {

    EditText et_nome;
    EditText et_cognome;
    EditText et_email;
    EditText et_password;
    EditText et_telefono;
    EditText et_indirizzo;
    Button btn_register;
    private FirebaseAuth mAuth;
    //FirebaseUser currentUser;
    DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_meccanico);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        //currentUser = FirebaseAuth.getInstance().getCurrentUser();

        //currentUser = mAuth.getCurrentUser();
        et_nome = findViewById(R.id.et_nome);
        et_cognome = findViewById(R.id.et_cognome);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_telefono = findViewById(R.id.et_telefono);
        et_indirizzo = findViewById(R.id.et_indirizzo_officina);

        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        toolbar.setTitle(getString(R.string.registerNow));
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        btn_register = findViewById(R.id.btn_register);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registerMeccanico(et_email, et_password);

                //scrittura di un record nella tabella users
                writeNewMeccanico(et_nome.getText().toString(), et_cognome.getText().toString(), et_email.getText().toString(),
                        et_password.getText().toString(), et_telefono.getText().toString(), et_indirizzo.getText().toString());
            }
        });
    }

    private void registerMeccanico(final EditText et_email, EditText et_password) {
        mAuth.createUserWithEmailAndPassword(et_email.getText().toString(), et_password.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            mAuth.getCurrentUser();
                            Intent main = new Intent(getApplicationContext(),MainActivity.class);
                            main.putExtra(getString(R.string.keyEmailMec),et_email.getText().toString());
                            Toast.makeText(getApplicationContext(),getString(R.string.success), Toast.LENGTH_SHORT).show();
                            startActivity(main);
                            finish();
                        } else {
                            // If sign in fails, display a message to the user.
                            //updateUI(null);
                            Toast.makeText(getApplicationContext(),getString(R.string.fail), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    //scrittura di un record nella tabella Meccanico
    private void writeNewMeccanico(String nome, String cognome, String email, String password, String telefono,
                              String indirizzo) {
        Meccanico meccanico = new Meccanico(nome, cognome, email, password, telefono, indirizzo);

        mDatabase.child(getString(R.string.meccanicoDB)).child(telefono).setValue(meccanico);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //per tornare nella activity precedente nel backstack
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
