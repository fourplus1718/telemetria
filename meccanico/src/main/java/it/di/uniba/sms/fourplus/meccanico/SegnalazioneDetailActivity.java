package it.di.uniba.sms.fourplus.meccanico;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import it.di.uniba.sms.fourplus.meccanico.classiDB.Anomalia;
import it.di.uniba.sms.fourplus.meccanico.classiDB.Cliente;

public class SegnalazioneDetailActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;
    private TextView textView_valoreCliente;
    private TextView textView_valoreClienteTel;
    private TextView textView_valoreTipoAnomalia;
    private TextView textView_valoreDescrizioneAnomalia;
    private TextView textView_valoreDataAnomalia;
    private TextView textView_valoreCodiceAnomalia;
    private TextView textView_valoreEmailCliente;
    Button setDate;
    Button setTime;
    Button bt_date;
    Button bt_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segnalazione_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        setSupportActionBar(toolbar);

        textView_valoreCliente = (TextView) findViewById(R.id.textView_valoreCliente);
        textView_valoreClienteTel = (TextView) findViewById(R.id.textView_valoreClienteTel);
        textView_valoreTipoAnomalia = (TextView) findViewById(R.id.textView_valoreTipoAnomalia);
        textView_valoreDescrizioneAnomalia = (TextView) findViewById(R.id.textView_valoreDescrizioneAnomalia);
        textView_valoreDataAnomalia = (TextView) findViewById(R.id.textView_valoreDataAnomalia);
        textView_valoreCodiceAnomalia = (TextView) findViewById(R.id.textView_valoreCodiceAnomalia);
        textView_valoreEmailCliente = (TextView) findViewById(R.id.textView_valoreEmailCliente);

        setDate = (Button) findViewById(R.id.setDataAppuntamento);
        setDate.setOnClickListener(showDatePickerDialog);

        setTime = (Button) findViewById(R.id.setOraAppuntamento);
        setTime.setOnClickListener(showTimePickerDialog);

        bt_date = (Button) findViewById(R.id.setDataAppuntamento);
        bt_time = (Button) findViewById(R.id.setOraAppuntamento);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent send_mess = new Intent(Intent.ACTION_SENDTO);
                String mess = getResources().getString(R.string.sendMessage);
                send_mess.putExtra("sms_body",mess);
                send_mess.setData(Uri.parse("smsto:"+textView_valoreClienteTel.getText().toString()));
                startActivity(send_mess);
                /*Snackbar.make(view, "Replace with your own detail action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mDatabase = FirebaseDatabase.getInstance().getReference();

        DatabaseReference ref = mDatabase.child(getString(R.string.anomaliaDB));
        Query anomaliaQuery = ref.orderByChild(getString(R.string.anomaliaCodiceDB)).equalTo(getIntent().getExtras().getString(getString(R.string.keyCodAnomaly)));
        anomaliaQuery.addValueEventListener(anomaliaListener);
    }

    ValueEventListener anomaliaListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            String email = "";
            Anomalia anomaliaLetta=null;
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                anomaliaLetta = singleSnapshot.getValue(Anomalia.class);
            }
            textView_valoreDataAnomalia.setText(""+anomaliaLetta.getDataAnomalia()+"    "+anomaliaLetta.getoraAnomalia());
            textView_valoreDescrizioneAnomalia.setText(""+anomaliaLetta.getValore()+" "+anomaliaLetta.getUnitàMisura());
            textView_valoreTipoAnomalia.setText(""+anomaliaLetta.getTipoAnomalia());
            textView_valoreCodiceAnomalia.setText(""+anomaliaLetta.getCodice());
            textView_valoreEmailCliente.setText(""+anomaliaLetta.getEmailCliente());
            email = anomaliaLetta.getEmailCliente();
            DatabaseReference ref = mDatabase.child(getString(R.string.utenteDB));
            Query anomaliaQuery = ref.orderByChild(getString(R.string.utenteEmailDB)).equalTo(email);
            anomaliaQuery.addValueEventListener(clienteListener);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    ValueEventListener clienteListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            Cliente clienteLetto=null;
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                clienteLetto = singleSnapshot.getValue(Cliente.class);

            }
            textView_valoreCliente.setText(""+clienteLetto.getCognome()+" "+clienteLetto.getNome());
            textView_valoreClienteTel.setText(""+clienteLetto.getTelefono());
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //per tornare nella activity precedente nel backstack
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener showDatePickerDialog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment newFragment = new SetDatePickerFragment();
            newFragment.show(getSupportFragmentManager(), "datePicker");
            //bt_date.setEnabled(false);
            bt_time = (Button) findViewById(R.id.setOraAppuntamento);
            bt_date = (Button) findViewById(R.id.setDataAppuntamento);
            if(!bt_date.isEnabled() && !bt_time.isEnabled()){
                DatabaseReference ref = mDatabase.child(getString(R.string.anomaliaDB));
                Query anomaliaQuery = ref.orderByChild(getString(R.string.anomaliaCodiceDB)).equalTo(textView_valoreCodiceAnomalia.getText().toString());
                anomaliaQuery.addValueEventListener(anomalyListener);
            }
        }
    };

    ValueEventListener anomalyListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot)
        {
            Anomalia anomaliaLetta=null;
            for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                anomaliaLetta = singleSnapshot.getValue(Anomalia.class);
            }
            anomaliaLetta.setAnomaliaIgnorataMeccanico(true);
            mDatabase.child(getString(R.string.anomaliaDB)).child(anomaliaLetta.getCodice()).setValue(anomaliaLetta);
        }
        @Override
        public void onCancelled(DatabaseError databaseError) {
            //Log.e(TAG, "onCancelled", databaseError.toException());
        }
    };

    View.OnClickListener showTimePickerDialog = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            bt_time = (Button) findViewById(R.id.setOraAppuntamento);
            bt_date = (Button) findViewById(R.id.setDataAppuntamento);
            if(!bt_date.isEnabled()){
                DialogFragment newFragment = new SetTimePickerFragment();
                newFragment.show(getSupportFragmentManager(), "timePicker");
                bt_time.setEnabled(false);
            }else{
                //mettere la stringa in strings
                Toast.makeText(getApplicationContext(),getString(R.string.selectDate), Toast.LENGTH_SHORT).show();
            }
            if(!bt_date.isEnabled() && !bt_time.isEnabled()){
                DatabaseReference ref = mDatabase.child(getString(R.string.anomaliaDB));
                Query anomaliaQuery = ref.orderByChild(getString(R.string.anomaliaCodiceDB)).equalTo(textView_valoreCodiceAnomalia.getText().toString());
                anomaliaQuery.addValueEventListener(anomalyListener);
            }
        }
    };

}
